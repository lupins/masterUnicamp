\documentclass{beamer}

\beamertemplatetransparentcoveredhigh
\setbeamertemplate{navigation symbols}{} % remove navigation symbols

\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{subfigure}
\usepackage{pgf}
\usepackage{perpage}
\MakePerPage{footnote}
\usepackage{booktabs}
%\usepackage{array}
\usepackage{multirow}
\usepackage{url}
\usepackage[outdir=figures/]{epstopdf}
\usepackage{tabularx}
% \usepackage{biblatex}
\usepackage[style=verbose-inote,citestyle=authortitle,backend=bibtex]{biblatex}
% \usepackage[style=authortitle,backend=bibtex]{biblatex}
%\usepackage{fnpct}
\usepackage{xpatch}
\xapptobibmacro{cite}{\setunit{\nametitledelim}\printfield{year}}{}{}
\addbibresource{eqm.bib}

% \graphicspath{{figures/}}

\usetheme{Madrid}
% \usetheme{Malmoe}

% \usecolortheme{seahorse}
% \usecolortheme{beaver}
\usecolortheme{dolphin}
% \usecolortheme{wolverine}

\title[Master's Qualifying Exam]{Human Fall Detection Based on Multi--Stream Convolutional Neural Networks}
\author[UNICAMP]{Guilherme Vieira Leite\\ Prof. Dr. Hélio Pedrini}

\institute[]{University of Campinas\\ Institute of Computing}
\subtitle{Master's Qualifying Exam}
\date{November 2018}

\titlegraphic{
    \vspace{1cm}
    \hspace{10cm}\includegraphics[width=1cm]{figures/logo-unicamp.png}
    \includegraphics[width=1cm]{figures/logo-ic-unicamp.png}
}

% set footnote size to tiny
\renewcommand{\footnotesize}{\tiny}
%\renewcommand{\footnotesize}{\scriptsize}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Summary}
  \tableofcontents
\end{frame}

\AtBeginSection[] % Do nothing for \section*
{
\begin{frame}
\frametitle{Summary}
\tableofcontents[currentsection]
\end{frame}
}

\section{Introduction}

\subsection{Problem Characterization}
\begin{frame}{Introduction}{Problem Characterization}
    \begin{itemize}
        \item World's population average age increase, life expectancy over 60 years.
        \item Health will dictate the quality of life in advanced age.
        \item Health problems rise with aging, such as muscular weakening.
        \item Muscular weakening facilitates the occurrence of falls.
    \end{itemize}
\end{frame}

\begin{frame}{Introduction}{Problem Characterization}
    \begin{itemize}
        \item Falls account for 646,000 deaths around the globe yearly.
        \item Chance to fall at least once a year:
        \begin{itemize}
            \item Over 60 year -- 28 to 35\%.
            \item Over 70 year -- 32 to 42\%.
        \end{itemize}
        \item Small fall can cause never healing injuries.
        \item These injuries affect self--confidence and independence.
  \end{itemize}
\end{frame}

\begin{frame}{Introduction}{Problem Characterization}
    \begin{itemize}
        \item Elder care is expensive.
        \item Current solution relies on hiring expensive care takers.
        \item Consequentially, families relocate them to their home.
        \item Striping elders from their independence and privacy.
    \end{itemize}
\end{frame}

\begin{frame}{Introduction}{Problem Characterization}
    \begin{itemize}
        \item Technological solution:
        \begin{itemize}
            \item An emergency system to detect a fall and call for help.
        \end{itemize}
        \item Most technological solutions attempt to attach devices on the body to detect the fall.
        \item Elderly's complaints about forgetting to use the device or that it is a nuisance to them.
    \end{itemize}
\end{frame}

\begin{frame}{Introduction}{Problem Characterization}
    \begin{itemize}
        \item Cameras as a cheap, non--intrusive and transparent solution.
        \item Returning to elderly their independence and freedom.
        \item Confidence to deal with the greatest hassle in advanced age.
    \end{itemize}
\end{frame}

\subsection{Contributions}
\begin{frame}{Introduction}{Contributions}
    \begin{itemize}
        \item A three--stream DNN classifier.
        \item Enables the emerging of an emergency system, capable of:
            \begin{itemize}
                \item Detect a fall;
                \item Raise alerts;
                \item Dispatch qualified help;
            \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Objectives}

\begin{frame}{Introduction}{Objectives}
    \begin{figure}[!htb]
        \centering
        \includegraphics[width=0.8\textwidth]{figures/emergency}\\
        \scriptsize{Diagram with the main components of the emergency computing system for monitoring activities and risk for the elderly people.}
        \label{fig:emergency}
    \end{figure}
\end{frame}

\begin{frame}{Introduction}{Objectives}
    \begin{itemize}
    \item<1-> \textbf{Research Questions}:
        \begin{itemize}
            \item<2-> Can handcrafted features maintain sufficient motion information so that deep networks are still able to extract complex temporal patterns from them?
            \item<3-> Is it possible to improve recognition accuracy by exploring multiple streams of information?
            \item<4-> How can we combine characteristics of distinctive nature to improved classification accuracy?
            \item<5-> Can the proposed method present successful performance across different datasets?
            \item<6-> Can this model be extended to different recognition scenarios?
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Background}

\subsection{Concepts}

\begin{frame}{Concepts}{Deep Neural Networks}
    \begin{itemize}
        \item Deep Neural Networks:
        \begin{itemize}
            \item A class of machine learning algorithms.
            \item Several layers of processing to extract and transform features.
            \item Each layer uses the output of the previous layer.
            \item Different architecture for different tasks.
        \end{itemize}
        \item Convolutional Neural Networks:
        \begin{itemize}
    	    \item Subtype of DNNs.
    	    \item Apply convolution operations to the entry.
    	    \item Resize the entry and feed it to the next layer.
    	    \item Fully connected layer at the end.
            \item Usually pre--trained in an extensive dataset.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Concepts}{CNN Architectures}
    \begin{itemize}
        \item VGG16:
        \begin{itemize}
            \item Small filters, 3$\times$3 convolutions.
            \item Two versions, VGG16 and VGG19. With 16 and 19 layers.
            \item Efficient when compared with 7$\times$7 filters of previous architectures.
            \item Fewer parameters in total, 138 million, small for a deep network.
        \end{itemize}
    \end{itemize}
    \begin{figure}[!htb]
        \centering
        \includegraphics[width=\textwidth]{figures/vgg16}\\
        \scriptsize{VGG16: 3$\times$3 convolutional layers, periodic pooling and a FC at the end.}
        \label{fig:vgg16}
    \end{figure}
\end{frame}

\begin{frame}{Concepts}{Inception}
    \begin{itemize}
        \item Inception:
        \begin{itemize}
            \item Goal was: a computationally efficient deep architecture.
            \item 22 layers.
            \item Introduced inception modules:
                \begin{itemize}
                    \item Creates a local topology.
                    \item A network inside a network.
                \end{itemize}
            \item Applies several filters in parallel.
            \item Concatenates the filter output and feeds to next layer.
        \end{itemize}
    \end{itemize}
    \begin{figure}[!htb]
        \centering
        \includegraphics[width=\textwidth]{figures/inception_0}\\
        \scriptsize{Inception: Bottlenecks keep features from growing too much.}
        \label{fig:inception}
    \end{figure}
\end{frame}

\begin{frame}{Concepts}{ResNet}
    \begin{itemize}
        \item ResNet:
        \begin{itemize}
            \item 152 layers.
            \item Residual learning:
            \begin{itemize}
                \item Instead of optimizing the identity mapping, it optimizes a residual mapping.
            \end{itemize}
        \end{itemize}
    \end{itemize}
    \begin{figure}[!htb]
        \centering
        \includegraphics[width=\textwidth]{figures/resnet}\\
        \scriptsize{Resnet: Curved arrows are residual learning shortcuts, dotted arrows are dimension reduction.}
        \label{fig:resnet}
    \end{figure}
\end{frame}

\begin{frame}{Concepts}{Fall}
    \begin{itemize}
        \item This work's fall definition:
        \begin{itemize}
            \item An unintentional movement that results with the person landing on the ground.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Concepts}{Optical Flow}
    \begin{itemize}
        \item A method for extracting a representation of the movement between two consecutive images.
        \item Represented as a vector of direction and magnitude.
    \end{itemize}
    \begin{figure}[!htb]
        \centering
        \setlength{\fboxsep}{1pt}
		\setlength{\fboxrule}{1pt}
        \fbox{\includegraphics[width=0.8\textwidth]{figures/opticalflow}}\\
        \scriptsize{Optical Flow.}
        \label{fig:opticalflow}
    \end{figure}
\end{frame}

\begin{frame}{Concepts}{Saliency Map}
    \begin{itemize}
        \item Technique for generating a map of attention regions on images.
        \item Can be calculated from keypoints alone or their movement.
    \end{itemize}
    \begin{figure}[!htb]
        \centering
        \setlength{\fboxsep}{1pt}
		\setlength{\fboxrule}{1pt}
        \fbox{\includegraphics[width=0.8\textwidth]{figures/saliencymap}}\\
        \scriptsize{Saliency Map.}
        \label{fig:saliencymap}
    \end{figure}
\end{frame}

\begin{frame}{Concepts}{Skeleton Pose}
    \begin{itemize}
        \item Technique that focuses on searching for the human skeleton shape and position.
        \item It has been done via depth image or joint tracking in RGB images.
    \end{itemize}
    \begin{figure}[!htb]
        \centering
        \setlength{\fboxsep}{1pt}
		\setlength{\fboxrule}{1pt}
        \fbox{\includegraphics[width=0.8\textwidth]{figures/skeleton}}\\
        \scriptsize{Skeleton Pose.}
        \label{fig:skeleton}
    \end{figure}
\end{frame}

\subsection{Related Work}
\begin{frame}{Related Work}
    \begin{itemize}
        \item Three categories:
        \begin{itemize}
            \item Wearable sensors.
            \item Ambiance sensors.
            \item Vision sensors.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Wearable Sensors}
    \begin{itemize}
        \item Wearable sensors on the subject's body to gather input data.
            \begin{itemize}
                \item Watches, accelerometers, gyroscopes,  smartphones.
            \end{itemize}
        \item Major drawback is the resistance of the subjects towards wearing the equipment.
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Wearable Sensors}
    \begin{itemize}
        \item Decision tree~\footcite{zhao2018}
        \item Dynamic time warping~\footcite{kumar2018}
        \item K-Nearest Neighbor\footnotemark[2]
        \item Threshold\footnotemark[2]$^,$~\footcite{kukharenko2017}
        \item Support Vector Machine\footnotemark[2]
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Wearable Sensors}
    \begin{figure}[!htb]
    \centering
    \setlength{\fboxsep}{1pt}
	\setlength{\fboxrule}{1pt}
    \subfloat{\fbox{\includegraphics[width=0.25\textwidth]{figures/zhao1} \label{fig:zhao1}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.25\textwidth]{figures/zhao2} \label{fig:zhao2}}} \\[0.2cm]
    \subfloat{\fbox{\includegraphics[width=0.25\textwidth]{figures/kumar} \label{fig:kumar}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.25\textwidth]{figures/kukharenko} \label{fig:kukharenko}}}
    \caption{Wearable devices.}
    \label{fig:wearable}
    \end{figure}
\end{frame}

\begin{frame}{Related Work}{Ambiance Sensors}
    \begin{itemize}
        \item Non--intrusive sensors that are not cameras.
        \item Alternative to wearable sensors.
        \item Alternative to cameras, privacy wise.
        \item Main setback is the high false positive (FP) rate.
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Ambiance Sensors}
    \begin{itemize}
        \item Vertical array of sensors to detect human presence on the top or bottom~\footcite{khin2017}.
        \item Accelerometer and microphone measuring vibrations on floor, classification done by a quadratic classifier~\footcite{zigel2009method}.
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Ambiance Sensors}
    \begin{figure}[!htb]
    \centering
    \setlength{\fboxsep}{1pt}
	\setlength{\fboxrule}{1pt}
    \subfloat{\fbox{\includegraphics[width=0.3\textwidth]{figures/zigel1} \label{fig:zigel1}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.3\textwidth]{figures/zigel2} \label{fig:zigel2}}}
    \caption{Dummy used by Zigel et al.~\footcite{zigel2009method}.}
    \label{fig:zigel}
    \end{figure}
\end{frame}

\begin{frame}{Related Work}{Vision Sensors}
    \begin{itemize}
        \item Images as input.
        \item Main appeal: non--intrusive, cheap.
        \item Main setback: blind spots.
    \end{itemize}
    \begin{itemize}
        \item They were subdivided into two groups, multiple cameras and single camera approach.
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Vision Sensors}
    \begin{itemize}
        \item Multiple Cameras
        \begin{itemize}
            \item Long Short--Term Memory~\footcite{shojaei-hashemi2018}
            \item Support Vector Machine~\footcite{mohd2017}
            \item Threshold~\footcite{nizam2017}$^,$~\footcite{mousse2017}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Related Work}{Vision Sensors}
    \begin{itemize}
        \item Single Camera
        \begin{itemize}
            \item AdaBoost~\footcite{zerrouki2018vision}
            \item CNN~\footcite{li2017fall}$^,$~\footcite{fan2018early}
            \item R--CNN~\footcite{min2018}
            \item RNN~\footcite{lie2018}
            \item SVM~\footcite{tran2017}$^,$~\footcite{fan2018human}
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Materials and Methods}
\subsection{Methodology}

\begin{frame}{Materials and Methods}{Methodology Training}
    \begin{figure}[!htb]
        \centering
        \includegraphics[width=\textwidth]{figures/methodology_tra}\\
        \scriptsize{Training}
        \label{fig:emergency}
    \end{figure}
\end{frame}

\begin{frame}{Materials and Methods}{Methodology Testing}
    \begin{figure}[!htb]
        \centering
        \includegraphics[width=\textwidth]{figures/methodology_tes}\\
        \scriptsize{Testing}
        \label{fig:emergency}
    \end{figure}
\end{frame}

\begin{frame}{Materials and Methods}{Junction}
    \begin{itemize}
        \item Junction:
        \begin{itemize}
            \item Three heterogeneous outputs from each CNN.
            \item Combining according to their relevance.
            \item Possible to join via: simple mean, weighted average or SVM.
            \item Chosen method: SVM
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Datasets}
\begin{frame}{Materials and Methods}{Datasets}
  \begin{itemize}
    \item URFD\footnote{\url{http://fenix.univ.rzeszow.pl/~mkepski/ds/uf}}
    \begin{itemize}
      \item 70 Videos -- 30 Falls -- 40 ADLs
      \item 30 fps -- 640$\times$240 pixels -- Various Lengths
      \item Annotation: Fall, Not Fall
    \end{itemize}
    \item Multicam\footnote{\url{http://www.iro.umontreal.ca/~labimage/Dataset}}
    \begin{itemize}
      \item 24 Scenarios -- 22 Falls -- 2 Not Falls
      \item 120 fps -- 720$\times$480 pixels -- Various Lengths
      \item Annotation: 9 Categories, first and last frame
    \end{itemize}
    \item FDD\footnote{\url{http://le2i.cnrs.fr/Fall-detection-Dataset}}
    \begin{itemize}
      \item 191 Videos -- 143 Falls -- 48 ADLs
      \item 25 fps -- 320$\times$240 pixels -- Various Lengths
      \item Annotation: First and last frame of fall
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Materials and Methods}{Datasets}
    \begin{figure}[!htb]
    \centering
    \setlength{\fboxsep}{1pt}
	\setlength{\fboxrule}{1pt}
    \subfloat{\fbox{\includegraphics[width=0.45\textwidth]{figures/urfd_1} \label{fig:urfd_1}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.45\textwidth]{figures/urfd_2} \label{fig:urfd_2}}} \\[0.2cm]
    \subfloat{\fbox{\includegraphics[width=0.45\textwidth]{figures/urfd_3} \label{fig:urfd_3}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.45\textwidth]{figures/urfd_4} \label{fig:urfd_4}}}
    \caption{URFD dataset}
    \label{fig:urfd}
    \end{figure}
\end{frame}

\begin{frame}{Materials and Methods}{Datasets}
    \begin{figure}[!htb]
    \centering
    \setlength{\fboxsep}{1pt}
	\setlength{\fboxrule}{1pt}
    \subfloat{\fbox{\includegraphics[width=0.4\textwidth]{figures/multicam_1} \label{fig:multicam_1}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.4\textwidth]{figures/multicam_2} \label{fig:multicam_2}}} \\[0.2cm]
    \subfloat{\fbox{\includegraphics[width=0.4\textwidth]{figures/multicam_3} \label{fig:multicam_3}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.4\textwidth]{figures/multicam_4} \label{fig:multicam_4}}}
    \caption{Multicam dataset}
    \label{fig:multicam}
    \end{figure}
\end{frame}

\begin{frame}{Materials and Methods}{Datasets}
    \begin{figure}[!htb]
    \centering
    \setlength{\fboxsep}{1pt}
	\setlength{\fboxrule}{1pt}
    \subfloat{\fbox{\includegraphics[width=0.35\textwidth]{figures/fdd_1} \label{fig:fdd_1}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.35\textwidth]{figures/fdd_2} \label{fig:fdd_2}}} \\[0.2cm]
    \subfloat{\fbox{\includegraphics[width=0.35\textwidth]{figures/fdd_3} \label{fig:fdd_3}}} \hspace*{0.1cm}
    \subfloat{\fbox{\includegraphics[width=0.35\textwidth]{figures/fdd_4} \label{fig:fdd_4}}}
    \caption{FDD dataset}
    \label{fig:fdd}
    \end{figure}
\end{frame}

\subsection{Computational Resources}
\begin{frame}{Materials and Methods}{Computational Resources}
  \begin{itemize}
    \item Python\footnote{https://www.python.org/}
    \begin{itemize}
      \item NumPy\footnote{\url{http://www.numpy.org/}}
      \item SciPy\footnote{\url{https://www.scipy.org/}}
      \item OpenCV\footnote{\url{http://opencv.org/}}
    \end{itemize}
    \item Tensorflow\footnote{https://www.tensorflow.org/}
    \item Keras\footnote{https://keras.io/}
    \item Amazon AWS
    \begin{itemize}
        \item g2.2xlarge, GPU nVidia GRID K520(Kepler), 8 vCPUs, 15GB of RAM
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Evaluation Metrics}
\begin{frame}{Materials and Methods}{Evaluation Metrics}
    \begin{equation}
    \label{eq:recall}
    \textit{Recall} = \frac{TP}{TP + FN}
    \end{equation}
    
    \begin{equation}
    \label{eq:specificity}
    \textit{Specificity} = \frac{TN}{TN + FP}
    \end{equation}
    
    \begin{equation}
    \label{eq:accuracy}
    \textit{Accuracy} = \frac{TP + TN}{TP + TN + FP + FN}
    \end{equation}
    
    \begin{equation}
    \label{eq:f_measure}
    F2 = (1 + 2^2) * \frac{\textit{Precision} * \textit{Recall}}{(2^2 * \textit{Precision}) + \textit{Recall}}
    \end{equation}  
\end{frame}

\section{Work Plan and Schedule}

\begin{frame}{Work Plan and Schedule}

    \begin{table}[!htb]
    \scriptsize
    \renewcommand{\tabcolsep}{2.5mm}
    \renewcommand{\arraystretch}{1.2}
    \centering
    \begin{tabular}{| l | c | c | c | c | c | c | c | c |}
    \hline
    \multirow{2}{*}{Activities}&\multicolumn{4}{c|}{1$^{\scriptsize \b o}$ year}&\multicolumn{4}{c|}{2$^{\scriptsize \b o}$ year}\\\cline{2-9}%\hline
    &1&2&3&4&1&2&3&4\\ \hline\hline
    {\bf Stage 1} &\multicolumn{8}{c|}{}\\\hline
    Literature review &$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$ \\\hline
    Dataset tweaking &&$\bullet$&$\bullet$&$\bullet$&$\bullet$&&& \\\hline
    {\bf Stage 2} &\multicolumn{8}{c|}{}\\\hline
    Selection of baseline neural network &&&$\bullet$&$\bullet$&$\bullet$&&& \\\hline
    Development of an end-to-end model &&&&$\bullet$&$\bullet$&$\bullet$&& \\\hline
    Experiments on proposed architecture &&&&&$\bullet$&$\bullet$&& \\\hline
    {\bf Stage 3} &\multicolumn{8}{c|}{}\\\hline
    Architecture refinement &&&&&$\bullet$&$\bullet$&& \\\hline
    Result analysis &&&&&$\bullet$&$\bullet$&$\bullet$& \\\hline
    {\bf Stage 4} &\multicolumn{8}{c|}{}\\\hline
    Result publication &&&&&&$\bullet$&$\bullet$&$\bullet$ \\\hline
    Writing &$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$&$\bullet$ \\\hline
    \end{tabular}
    
    \caption{Activity list for two-year Master's degree split into trimesters.}
    \label{tab:schedule}
    
    \end{table}

\end{frame}

\begin{frame}{Thank You}
    \centering
    Thank You
\end{frame}

\end{document}

