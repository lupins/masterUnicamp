\chapter{Conceitos e Trabalhos Relacionados}
\label{chap:background}

Neste capítulo, os conceitos que foram utilizados na metodologia proposta são descritos em detalhes. Além disso, os trabalhos revisados durante a execução da tese são reportados na segunda parte do capítulo.

\section{Redes Neurais Profundas}

As redes neurais profundas (DNNs) são uma classe de algoritmos de aprendizado de máquina, em que várias camadas de processamento são utilizadas para extrair e transformar características dos dados de entrada e o aprendizado dos neurônios da rede ocorre pela aplicação do algoritmo de retropropagação (\textit{backpropagation}). As informações de entrada de cada camada são as mesmas da saída da camada anterior, exceto na primeira camada, onde entram os dados externos, e na última camada, de onde são extraídos os resultados do processamento~\cite{goodfellow2016}. Essa estrutura não é necessariamente fixa, algumas camadas podem ter duas outras camadas como entrada ou várias saídas.

Deng e Yu~\cite{deng2014deep} citam alguns motivos pela crescente popularidade das redes profundas nos últimos anos, os quais incluem seus resultados nos problemas de classificação, melhorias nas Unidades de Processamento Gráfico (GPUs), aparecimento de Unidades de Processamento de Tensores (TPUs) e a quantidade de dados disponíveis digitalmente.

As camadas de uma rede profunda podem ser organizadas de diversas maneiras, a depender da necessidade de cada tarefa. A maneira na qual uma rede profunda é organizada é chamada de arquitetura e algumas delas se tornaram bem conhecidas devido aos resultados na classificação de imagens. Algumas delas são: AlexNet~\cite{krizhevsky2012}, LeNet~\cite{lecun1998gradient}, VGGNet~\cite{simonyan2014deep} e ResNet~\cite{he2016deep}.

\section{Redes Neurais Convolucionais}

As redes neurais convolucionais (CNNs) são um subtipo de redes profundas, em que sua estrutura é semelhante a de uma DNN, tal que a informação flui de uma camada para a próxima. Porém, no caso das CNNs, os dados passam pelas camadas convolucionais, que aplicam várias operações de convolução e redimensionam os dados, antes de serem repassados para a próxima camada.

As operações de convolução permitem que a rede aprenda características de baixo nível nas primeiras camadas, e combine-as nas camadas seguintes para aprender características de alto nível. Apesar de não ser obrigatório, geralmente no final de uma rede convolucional existem algumas camadas totalmente conexas. 

No contexto deste trabalho, duas arquiteturas de CNNs são relevantes: (i) VGG-16~\cite{simonyan2014deep} e (ii) Inception~\cite{szegedy2015going}. A VGG-16 foi a vencedora da competição Desafio de Reconhecimento Visual em Larga Escala - ImageNet 2014 (ILSVRC), com um erro de 7,3\% na categoria de localização. Sua característica de utilizar filtros pequenos, convoluções de 3$\times$3, {\it stride} 1, {\it padding} 1 e {\it max pooling} de 2$\times$2 com {\it stride} 2, permitiu que a rede fosse mais profunda, sem torná-la computacionalmente proibitiva. A VGG-16 possui 16 camadas e 138 milhões de parâmetros, o que é considerado pouco para redes profundas. A maior carga das computações desta rede ocorre nas primeiras camadas, pois, a partir delas, as camadas de \textit{pooling} reduzem consideravelmente a carga das camadas mais profundas. A Figura~\ref{fig:arch_vgg16} ilustra a arquitetura da VGG-16.

\begin{figure}[!htb]
\centering
\includegraphics[width=14.0cm]{figures/back_arch_vgg}
\caption[Arquitetura VGG-16]{Ilustração das camadas da arquitetura VGG-16.}
\label{fig:arch_vgg16}
\end{figure}

A segunda arquitetura, Inception V1~\cite{szegedy2015going}, foi a vencedora da ILSVRC 2014, mesmo ano da VGG-16, porém, venceu na categoria de classificação, com um erro de 6,7\%. Esta rede foi desenvolvida para ser mais profunda e, ao mesmo tempo, computacionalmente mais eficiente. Como ilustrada na Figura~\ref{fig:arch_inception}, a rede possui 22 camadas e somente 5 milhões de parâmetros. Sua construção consiste no empilhamento de vários módulos, chamados de Inception, ilustrados na Figura~\ref{fig:arch_inception_naive}.

\begin{figure}[!htb]
\centering
\includegraphics[width=15.8cm]{figures/back_arch_inception}
\caption[Arquitetura Inception]{Ilustração das camadas da arquitetura Inception. Fonte: Szegedy et al.~\cite{szegedy2015going}.}
\label{fig:arch_inception}
\end{figure}

Os módulos foram pensados de forma a criar algo como uma rede dentro da rede, em que várias operações de convolução e \textit{max pooling} são executadas paralelamente e, ao final destas operações, as características são concatenadas para ser enviadas ao próximo módulo. Entretanto, se a rede fosse composta por módulos Inception, como ilustrado na Figura~\ref{fig:arch_inception_naive}, ela executaria 850 milhões de operações. Para reduzir este número, os gargalos foram criados. Os gargalos reduzem o número de operações para 358 milhões. Eles são convoluções 1$\times$1 que preservam a dimensão espacial, ao mesmo tempo que diminuem a profundidade das características. Eles foram alocados antes das convoluções 3$\times$3, 5$\times$5 e após o \textit{max pooling} 3$\times$3, como ilustrado na Figura~\ref{fig:arch_inception_bottle}.

\begin{figure}[!htb]
\centering
\subfloat[]{\includegraphics[width=8.0cm]{figures/back_inception_naive} \label{fig:arch_inception_naive}} \hspace*{0.1cm}
\subfloat[]{\includegraphics[width=8.0cm]{figures/back_inception_bottle} \label{fig:arch_inception_bottle}}
\caption[Módulo Inception]{Ilustrações do módulo Inception. (a) módulo sem gargalo; (b) módulo com gargalo. Fonte: Szegedy et al.~\cite{szegedy2015going}.}
\end{figure}

\subsection{Transferência de Aprendizado}

A transferência de aprendizado consiste no reaproveitamento de uma rede cujos pesos foram treinados em um outro contexto. Além de melhorar os resultados das redes que a utilizam, a técnica também colabora para diminuir o tempo necessário de convergência e suprir casos em que não há dados o suficiente para treinar a rede inteira~\cite{zhuang2019comprehensive}.

Tipicamente em problemas de classificação, as redes são previamente treinadas na base de dados ImageNet~\cite{deng2009imagenet}, que é uma das maiores e mais conhecidas bases de dados. O objetivo é que a rede aprenda padrões complexos e genéricos o suficiente, de forma que sejam úteis a novos problemas.

\section{Definição de Queda}

A literatura não apresenta uma definição universal de queda~\cite{Tasoulis2019,luna2019automated,kong2019learning}, todavia, alguns órgãos de saúde criaram suas próprias definições, as quais podem ser utilizadas para descrever uma ideia geral de queda.

A instituição americana \textit{Joint Commission}~\cite{jtc} define uma queda como ''[...] uma mudança não intencional da posição, que termina ao chão ou a alguma superfície baixa (por exemplo, uma cama, cadeira ou um tapete). [...]''. A Organização Mundial da Saúde~\cite{who_fact} define como ''[...] um evento cujo resultado é a pessoa estar inadvertidamente no chão ou algum nível mais baixo [...]''. A definição do Centro Nacional de Assuntos de Veteranos~\cite{va_fall} é a ''Perda da postura ereta, resultando no descanso ao chão, objeto ou mobília, ou uma repentina, incontrolada, involuntária disposição do corpo na direção do chão, ou objeto próximo, como uma cadeira ou escada [...]''. Neste trabalho nós descrevemos uma queda como um movimento involuntário que resulta em um indivíduo indo em direção ao chão.

\section{Fluxo Óptico}

O fluxo óptico é uma técnica que deduz representações de movimentos de pixels causados pelo deslocamento do objeto ou da câmera. O vetor que representa o movimento é o mesmo para uma vizinhança de pixels e é esperado que os pixels não saiam da área do quadro. O fluxo óptico é um método local, o que implica a dificuldade de seu cálculo em regiões uniformes.

O cálculo do fluxo óptico é realizado a partir da comparação de dois quadros consecutivos e sua representação é um vetor de direção e magnitude. Considere $I$ um quadro de vídeo e $I(x, y, t)$ um pixel neste quadro. Um quadro analisado em um tempo futuro $dt$ é descrito na Equação~\ref{eq:of_frame} em função de um deslocamento $(dX, dY)$ do pixel $I(x, y, t)$. A Equação~\ref{eq:of_gradient} é obtida de uma série de Taylor dividida por $dt$ e apresenta os gradientes $f_t$, $f_x$ e $f_y$ (Equação~\ref{eq:fx_fy}). Os componentes do fluxo óptico são os valores de $u$ e $v$ (Equação~\ref{eq:of_variables}) e podem ser obtidos por diversos métodos como o de Lucas-Kanade~\cite{lucas1981iterative} ou Gunnar-Farnebäck~\cite{farneback2003two}. A Figura~\ref{fig:ex_flow} ilustra alguns quadros de fluxo óptico.
\begin{gather}
I(x, y, t) = I(x + dX, y + dY, t + dT) \label{eq:of_frame} \\
f_xu + f_yv + f_t = 0 \label{eq:of_gradient} \\
f_x = \frac{\partial f}{\partial x} \qquad f_y = \frac{\partial f}{\partial y} \qquad f_t = \frac{\partial f}{\partial t} \label{eq:fx_fy} \\
u = \frac{\partial x}{\partial t} \qquad v = \frac{\partial y}{\partial t} \label{eq:of_variables}
\end{gather}

\begin{figure}[!htb]
\centering
\includegraphics[width=15.8cm]{figures/back_optical_flow}
\caption[Exemplos de fluxo óptico]{Exemplos de fluxo óptico. Cada fluxo óptico foi extraído entre o quadro ilustrado e o próximo na sequência. A cor do pixel indica a direção do movimento e o seu brilho indica a magnitude.}
\label{fig:ex_flow}
\end{figure}

\section{Ritmo Visual}

Ritmo visual é uma técnica de codificação, com o objetivo de obter a informação temporal de um vídeo, sem perder a informação espacial. Sua representação consiste em uma única imagem correspondendo ao vídeo inteiro, de forma que cada quadro de vídeo contribui para uma coluna da imagem final~\cite{moreira2017first, torres2018detection, valio2011fast}.

\begin{figure}[!htb]
\centering
\subfloat{\includegraphics[width=4.5cm]{figures/back_rhythm_zig_zag} \label{fig:rhythm_zig-zag}} \hspace*{0.3cm}
\subfloat{\includegraphics[width=4.5cm]{figures/back_rhythm_columns} \label{fig:rhythm_columns}}
\caption[Construção do ritmo visual]{Processo de construção do ritmo visual. (a) forma em zigue-zague na qual cada quadro de vídeo é percorrido; (b) Construção do ritmo pela concatenação das colunas, obtidas pelo zigue-zague. Fonte:~\cite{valio2011fast}.}
\label{fig:rhythm_construction}
\end{figure}

Para a construção do ritmo visual, cada quadro de vídeo é percorrido em zigue-zague, da sua diagonal esquerda inferior até sua diagonal direita superior, como ilustrado na Figura~\ref{fig:rhythm_zig-zag}. Cada quadro processado em zigue-zague gera uma coluna de pixels, a qual é concatenada com as outras colunas para formar o ritmo visual (Figura~\ref{fig:rhythm_columns}). As dimensões da imagem de ritmo são de $W \times H$, em que a largura $W$ é o número de quadros do vídeo e a altura $H$ é o tamanho do caminho percorrido. A Figura~\ref{fig:ex_rhythm} ilustra alguns ritmos visuais extraídos.

\begin{figure}[!htb]
\centering
\includegraphics[width=15.8cm]{figures/back_rhythm}
\caption[Exemplos de ritmo visual]{Exemplos de ritmo visual. Um quadro de cada vídeo é mostrado na parte superior e o correspondente ritmo visual do vídeo é apresentado na parte inferior da figura.}
\label{fig:ex_rhythm}
\end{figure}

\section{Mapa de Saliência}

A saliência, no contexto de processamento de imagens, é uma característica da imagem que representa localizações de regiões na imagem. As regiões latentes são geralmente apresentadas em tons de cinza, regiões sem características ativadas são representadas por pixels pretos e a região mais latente pelo pixel branco. No contexto de aprendizado profundo, o mapa de saliência é o mapa da contribuição de cada pixel da imagem no processo de classificação. Inicialmente, a saliência foi extraída como uma forma de se compreender o que as redes profundas estavam de fato aprendendo e ainda é utilizada desta forma, como no trabalho de Li et al.~\cite{li2017beyond}.

O mapa de saliência foi utilizado por Zuo et al.~\cite{zuo2019enhanced} como característica para a classificação de ações egocêntricas, em que a fonte de informação é uma câmera que corresponde à visão em primeira pessoa do sujeito. A utilização da saliência na classificação de ações vem da suposição de que o importante de uma ação ocorre à frente dos quadros de vídeo, em vez de ocorrer ao fundo.

O mapa de saliência pode ser obtido de diversas maneiras, tal como mostrado por Smilkov et al.~\cite{smilkov2017smoothgrad}, Sundararajan et al.~\cite{sundararajan2017axiomatic} e Simonyan et al.~\cite{simonyan2013deep}. A Figura~\ref{fig:ex_saliency} ilustra a extração do mapa de saliência.

\begin{figure}[!htb]
\centering
\includegraphics[width=15.8cm]{figures/back_saliency}
\caption[Exemplo de mapas de saliência]{Exemplos de mapas de saliência. Os pixels variam do preto ao branco, de acordo com sua importância para a classificação, branco sendo a maior relevância.}
\label{fig:ex_saliency}
\end{figure}

\section{Estimação de Pose}

É uma técnica de derivação da postura de um ou mais seres humanos. Diferentes sensores são utilizados como entrada para essa técnica, como os sensores de profundidade de um Microsoft Kinect ou imagens de uma câmera.

O algoritmo proposto por Cao et al.~\cite{cao2017realtime}, o OpenPose, é notável pela sua eficácia ao estimar a pose de indivíduos em quadros de vídeo. O OpenPose opera com uma rede de dois estágios em busca de 18 juntas do corpo. No primeiro estágio, o método cria mapas de confiança das posições das juntas e o segundo estágio prediz campos de afinidade entre as partes encontradas. A afinidade é representada por um vetor 2D, que codifica a posição e a orientação de cada membro do corpo. A Figura~\ref{fig:ex_pose} exibe a extração da postura de alguns quadros.

\begin{figure}[!htb]
\centering
\includegraphics[width=15.8cm]{figures/back_pose}
\caption[Exemplo de estimação de pose]{Exemplo da extração da estimação de postura. Os círculos representam as juntas encontradas e as arestas os membros. Cada membro é associado a uma cor fixa.}
\label{fig:ex_pose}
\end{figure}

\section{Literatura Relacionada}

Nas subseções a seguir, nós elucidamos os trabalhos relacionados à detecção de quedas e seus métodos. Os trabalhos foram divididos em dois grupos, baseados nos sensores utilizados: (i) métodos sem vídeos e (ii) métodos com vídeos.

\subsection{Métodos Sem Vídeos}

Neste grupo, encontram-se os trabalhos que utilizam diversos tipos de sensores para obtenção de dados, podendo ser um relógio, acelerômetro, giroscópio, sensor de frequência cardíaca ou um {\it smartphone}, ou seja, qualquer sensor que não utilize uma câmera. 

Khin et al.~\cite{khin2017development} desenvolveram um sistema de triangulação que utiliza vários sensores de presença instalados no cômodo monitorado de uma casa. A presença de alguém no cômodo causaria uma pertubação nos sensores e uma queda ativaria outro padrão de ativação. Apesar de não reportarem os resultados, os autores afirmaram que testaram essa configuração e que os sensores detectaram padrões diferentes para ações diferentes, indicando que a solução poderia ser utilizada para detectar quedas.

\begin{figure}[!htb]
\centering
\includegraphics[width=10.0cm]{figures/back_kukharenko}
\caption[Hardware de Kukharenko e Romanenko~\cite{kukharenko2017picking}]{Ilustração do dispositivo proposto por Kukharenko e Romanenko~\cite{kukharenko2017picking}.}
\label{fig:kukha}
\end{figure}

Kukharenko e Romanenko~\cite{kukharenko2017picking} obtiveram seus dados de um dispositivo pulseira, ilustrado na Figura~\ref{fig:kukha}. Um limiar foi utilizado para detectar impacto ou um estado ``sem peso'' e, após esta detecção, o algoritmo espera por um segundo e analisa as informações obtidas. O método foi testado em alguns voluntários, que relataram reclamações tais como esquecer de vestir o dispositivo e o desconforto que o mesmo causava.

Kumar et al.~\cite{kumar2018wearable} colocaram um sensor preso a um cinto na pessoa (Figura~\ref{fig:kumar}) e compararam quatro métodos para detectar quedas: limiar, Máquinas de Vetores de Suporte (SVM), $K$ vizinhos mais próximos (KNN) e \textit{Dynamic Time Warping} (DTW). Os autores também comentaram a importância de sensores acoplados ao corpo, uma vez que eles monitorariam o indivíduo constantemente e não sofreriam com pontos cegos das câmeras.

\begin{figure}[!htb]
\centering
\includegraphics[width=12.0cm]{figures/back_kumar}
\caption[Hardware de Kumar et al.~\cite{kumar2018wearable}]{Ilustração da simulação de queda e do dispositivo utilizado por Kumar et al.~\cite{kumar2018wearable}.}
\label{fig:kumar}
\end{figure}

Vallejo et al.~\cite{vallejo2013artificial} desenvolveram uma rede neural profunda para classificar os dados do sensor. O sensor em questão é um giroscópio vestido na cintura, ilustrado na Figura~\ref{fig:vallejo}, e a rede é composta de três camadas ocultas, com cinco neurônios cada uma. Os autores realizaram experimentos com adultos de 19 a 56 anos.

\begin{figure}[!htb]
\centering
\includegraphics[width=9.0cm]{figures/back_vallejo}
\caption[Hardware de Vallejo et al.~\cite{vallejo2013artificial}]{Ilustração do dispositivo utilizado por Vallejo et al.~\cite{vallejo2013artificial}.}
\label{fig:vallejo}
\end{figure}

Zhao et al.~\cite{zhao2018recognition} coletaram dados de um giroscópio acoplado à cintura do indivíduo, ilustrado na Figura~\ref{fig:zhao}, e utilizaram uma árvore de decisão para classificar as informações. Os experimentos foram executados em cinco adultos aleatórios.

\begin{figure}[!htb]
\centering
\includegraphics[width=12.0cm]{figures/back_zhao}
\caption[Hardware de Zhao et al.~\cite{zhao2018recognition}]{Ilustração do giroscópio, acoplado à cintura, utilizado por Zhao et al.~\cite{zhao2018recognition}.}
\label{fig:zhao}
\end{figure}

Zigel et al.~\cite{zigel2009method} utilizaram acelerômetros e microfones como sensores, porém, os sensores foram instalados no ambiente, em vez de acoplados ao sujeito. Os sensores detectaram vibrações e alimentaram um classificador quadrático. Os testes foram feitos com um boneco de testes, que era solto de uma posição ereta, exibido na Figura~\ref{fig:zigel}.

\begin{figure}[!htb]
\centering
\includegraphics[width=12.0cm]{figures/back_zigel}
\caption[Experimento de Zigel et al.~\cite{zigel2009method}]{Ilustração da experimentação com um boneco de testes por Zigel et al.~\cite{zigel2009method}.}
\label{fig:zigel}
\end{figure}

\subsection{Métodos Com Vídeos}

Nesta seção, os métodos foram agrupados cuja fonte principal de dados é formada por sequências de vídeos. Apesar da semelhança nos sensores, os métodos apresentam uma variedade de soluções como, por exemplo, os trabalhos a seguir que utilizam técnicas de ativação baseadas em limiar.

% -- THRESHOLD
Para isolar a silhueta humana, Lee e Mihailidis~\cite{lee2005intelligent} realizaram uma subtração do fundo em conjunto com uma extração de regiões. A partir disso, a postura foi determinada por meio de um limiar dos valores do perímetro da silhueta, velocidade do centro da silhueta e pelo diâmetro de Feret. A solução foi testada em uma base criada pelos autores.

Nizam et al.~\cite{nizam2017human} usaram dois limiares, o primeiro verifica se a velocidade do corpo é alta e, neste caso, o segundo limiar verifica se a posição das juntas estão próximas ao chão. As informações das juntas foram obtidas após uma subtração do fundo, por meio de uma câmera Kinect. Os experimentos foram realizados em um conjunto de dados criado pelos autores.

Sase e Bhandari~\cite{sase2018human} utilizaram um limiar em que, se a região de interesse fosse menor do que um terço do corpo do indivíduo, uma queda era detectada. A região de interesse foi obtida pela extração do fundo e o método foi testado na base URFD~\cite{kwolek2014human}. Bhandari et al.~\cite{bhandari2017novel} aplicaram um limiar sobre a velocidade e a direção da região de interesse. Uma combinação de Shi-Tomasi com Lucas-Kanade foi aplicada para determinar a região de interesse. Os autores testaram a abordagem no conjunto URFD~\cite{kwolek2014human} e reportaram 95\% de acurácia.

% -- SVM
Outra técnica de classificação bastante utilizada é o SVM, empregada por Abobakr et al.~\cite{abobakr2017skeleton}. O método utilizou informações de profundidade para subtrair o fundo do quadro do vídeo, aplicou um algoritmo de floresta aleatória para estimar a postura e a classificou com SVM. Fan et al.~\cite{fan2018human} também separaram o quadro entre frente e fundo e encaixaram uma elipse na silhueta do corpo humano encontrado. A partir da elipse, seis características foram extraídas e serviram para alimentar uma função \textit{slow feature}. As saídas dessa função passaram por um classificador SVM e foram testadas na base de dados SDUFall~\cite{ma2014depth}.

Harrou et al.~\cite{harrou2017vision} utilizaram um classificador SVM que recebe características extraídas dos quadros de vídeo. Durante os testes, os autores compararam o SVM com uma \textit{multivariate exponentially-weighted moving average} (MEWMA) e testaram a solução nas bases URFD~\cite{kwolek2014human} e FDD~\cite{charfi2013optimised}.

Mohd et al.~\cite{mohd2017optimized} alimentaram um classificador SVM com as informações de altura, velocidade, aceleração e posição das juntas do corpo humano e realizaram testes em três bases de dados: TST Fall Detection~\cite{gasparrini2015proposal}, URFD~\cite{kwolek2014human} e Fall Detection by Zhang~\cite{zhong_fall}. Panahi e Ghods~\cite{panahi2018human} subtraíram o fundo a partir das informações de profundidade, encaixaram uma elipse na forma do indivíduo, classificaram a elipse com SVM e realizaram testes no conjunto de dados URFD~\cite{kwolek2014human}.

% -- Privacy
Preocupados com a privacidade dos usuários, os trabalhos a seguir defendem que as soluções para detectar quedas devem oferecer opções de anonimidade. Dessa forma, Edgcomb e Vahid~\cite{edgcomb2012automated} testaram a efetividade de um classificador árvore binária, sobre uma série temporal. Os autores compararam diversas formas de se esconder a identidade, como borramento, extração da silhueta, substituição do indivíduo por uma elipse opaca ou por uma caixa opaca. Eles realizaram testes em uma base própria, com 23 vídeos gravados. Lin et al.~\cite{lin2013fall} investigaram uma solução focada em privacidade com a utilização de silhueta. Eles aplicaram um classificador KNN somado a um temporizador que verifica se a pose do indivíduo voltou ao normal. Os testes foram realizados por voluntários do laboratório.

%  -- CNN
Alguns trabalhos utilizaram redes neurais convolucionais, como o caso de Anishchenko~\cite{anishchenko2018machine}, que implementou uma adaptação da arquitetura AlexNet para detectar quedas no conjunto de dados FDD~\cite{charfi2013optimised}. Fan et al.~\cite{fan2018early} utilizaram uma CNN para monitorar e avaliar o grau de completude de um evento. Uma pilha de quadros de um vídeo foi utilizada em uma arquitetura VGG-16 e seu resultado foi associado com o primeiro quadro da pilha. O método foi testado em dois conjuntos de dados: FDD~\cite{charfi2013optimised} e Hockey Fights~\cite{nievas2011violence}. Seus resultados foram reportados em termos de completude das quedas.

Huang et al.~\cite{huang2018video} fizeram o uso do algoritmo OpenPose para obter as coordenadas das juntas do corpo. Dois classificadores (SVM e VGG-16) foram comparados para classificar as coordenadas. Os experimentos foram realizados nas bases URFD~\cite{kwolek2014human} e FDD~\cite{charfi2013optimised}. Li et al.~\cite{li2017fall} criaram uma modificação da arquitetura de CNN, AlexNet. A solução foi testada no conjunto de dados URFD~\cite{kwolek2014human}, além disso os autores também reportaram que a solução classificou entre ADLs e quedas em tempo real.

Min et al.~\cite{min2018detection} utilizaram uma R-CNN (CNN de regiões) para analisar uma cena, que gera relações espaciais entre a mobília e o ser humano em cena, tal que a relação espacial é classificada. Os autores experimentaram em três conjuntos de dados: o URFD~\cite{kwolek2014human}, KTH~\cite{schuldt2004recognizing} e uma base criada por eles. N\'u\~nez-Marcos et al.~\cite{nunez2017vision} realizaram a classificação com uma VGG-16. Os autores calcularam o fluxo óptico denso, que serviu de característica para a rede classificar. Eles testaram o método nas bases de dados URFD~\cite{kwolek2014human} e FDD~\cite{charfi2013optimised}.

% -- RNN
Coincidentemente, todos os trabalhos que utilizaram redes neurais recorrentes fizeram o uso da mesma arquitetura. Lie et al.~\cite{lie2018human} aplicaram uma rede neural recorrente, com células LSTM ({\it Long Short-Term Memory}), para classificar a postura do indivíduo. A postura foi extraída por uma CNN e os experimentos foram realizados em uma base de dados criada pelos autores.

Shojaei-Hashemi et al.~\cite{shojaei2018video} utilizaram um aparelho Microsoft Kinect para obter a informação de postura do indivíduo e uma rede neural recorrente LSTM como classificador. Os experimentos foram realizados no conjunto de dados NTU RGB+D. Além disso, os autores relataram a utilização do Kinect como uma vantagem, pois a extração da postura pode ser realizada em tempo real. Lu et al.~\cite{lu2018deep} propuseram a aplicação de uma LSTM logo em seguida de uma CNN 3D. Os autores realizaram testes nas bases URFD~\cite{kwolek2014human} e FDD~\cite{charfi2013optimised}.

% -- K-NN
Outros algoritmos de aprendizado de máquina, como o K-vizinhos mais próximos também foram encontrados para o problema de detecção de quedas. Kwolek e Kepski~\cite{kwolek2015improving} fizeram o uso de uma combinação entre acelerômetro e Kinect. Durante a suspeita de uma queda, o acelerômetro ultrapassa um limiar e, a partir daí, a câmera Kinect começa a capturar quadros de profundidade da cena. Os autores compararam a classificação dos quadros entre KNN e SVM e testaram no conjunto de dados URFD~\cite{kwolek2014human} e em uma base independente.

Sehairi et al.~\cite{sehairi2018elderly} desenvolveram uma máquina de estados finita, para estimar a posição da cabeça do ser humano a partir da silhueta extraída. Os testes foram realizados na base de dados FDD~\cite{charfi2013optimised}.

% -- Markov
A aplicação de filtros de Markov também foi utilizada na detecção de quedas, como no trabalho de Anderson et al.~\cite{anderson2006recognizing}, em que a silhueta do indivíduo foi extraída para que suas características fossem classificadas pelo filtro de Markov. Os experimentos foram realizados em bases próprias.

Zerrouki e Houacine~\cite{zerrouki2018combined} descreveram as características do corpo por meio de coe\-fi\-cien\-tes de curvelet e da razão entre as áreas do corpo. Um classificador SVM realizou a identificação da postura e o filtro de Markov discriminou entre quedas ou não quedas. Os autores reportaram experimentações nos conjuntos de dados URFD~\cite{kwolek2014human} e FDD~\cite{charfi2013optimised}.

% -- Others
Além dos métodos citados anteriormente, os seguintes trabalhos fizeram uso de diversas técnicas, como Yu et al.~\cite{yu2010robust}, que obtiveram suas características pela aplicação de técnicas de rastreamento de cabeça e análise de variação de forma. As características serviram de entrada a um classificador Gaussiano. Os autores criaram uma base própria para os testes. Zerrouki et al.~\cite{zerrouki2018vision} segmentaram os quadros entre frente e fundo e aplicaram mais uma segmentação sobre o corpo humano, dividindo-o em cinco partições. As segmentações do corpo foram passadas a um classificador AdaBoost, que obteve 96\% de acurácia no conjunto URFD~\cite{kwolek2014human}. Finalmente, Xu et al.~\cite{xu2018new} publicaram um \textit{survey} avaliando diversos sistemas de detecção de quedas.
