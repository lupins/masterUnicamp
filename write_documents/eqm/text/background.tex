\chapter{Background}
\label{cap:background}

This chapter is divided into two main topics. Initially, it is clarified some of the concepts used during the study regarding fall detection, which are needed to understand the following sections of this work. Afterwards, we describe some relevant work, found in the literature, related to the research topic.

\section{Concepts}
\label{sec:concept}

In this section, we explain some of the relevant concepts and techniques related to the theme under investigation associated with deep neural networks, fall, optical flow, saliency map and skeleton pose.

\subsection{Deep Neural Network}

Deep Neural Networks (DNNs) are a class of machine learning algorithms that use several layers of processing to extract and transform features, where each layer uses the output of the previous layer as input~\cite{goodfellow2016}. DNNs experienced a growth in popularity in the last few years due to their performance on image classification problems, increased computing power by Graphics Processing Units (GPUs), and amount of raw data available digitally~\cite{deng2014}.

DNNs use an internal architecture to perform different tasks, and some of these architectures are noticeably famous for their results on image classification problems, such as AlexNet~\cite{krizhevsky2012}, LeNet~\cite{lecun1998}, Inception~\cite{szegedy2017}, ResNet~\cite{he2016}, and VGGNet~\cite{simonyan2014very}. Since we intend to experiment with Inception, Resnet, and VGGNet, they will be further explained in the following subsections.

\subsubsection{CNN}

Convolution Neural Networks (CNNs) are a subtype of DNNs commonly used in image segmentation, object classification, feature extraction, voice recognition, and video classification~\cite{geron2017}. A CNN is composed of a similar structure of a DNN, that is, several layers that feed the next layer with their output.

In a CNN, the initial layers usually apply several convolution operations to the entry, resize it, and feed it to the next convolution layer and, at the end, there is a fully connected network. It is also common to pre-train the CNN with a broad dataset, for instance, ImageNet~\cite{deng2009imagenet}, and transfer the knowledge, via weights or parameters, to another context, and fine-tune the CNN to the specific problem. This usually helps by increasing the accuracy of the solution.

\subsubsection{VGG16}

VGGNet~\cite{simonyan2014very} became popular for being a deep network that used only small filters, 3$\times$3 convolutions. It is known on its two versions VGG16 and VGG19, which are respectively 16 and 19 layers deep. The choice to use only small features was to be efficient, resource wise. When compared with 7$\times$7 filters of previously proposed architectures, VGGNet has fewer parameters in total (138 million), which is considered small for a deep network.

This architecture concentrates its computational load on the initial convolutional layers and the load is reduced considerably as it advances deeper. It uses a simple design, illustrated in Figure~\ref{fig:vgg16}, with 3$\times$3 convolutional layers and a periodic pooling between them and has a fully connected network at the end.

\begin{figure}[!htb]
\centering
\includegraphics[width=14.5cm]{vgg16}
\caption{VGG16~\cite{simonyan2014very}.}
\label{fig:vgg16}
\end{figure}

The reputation of this architecture is derived from how well it is able to generalize its application, and that is due to its fully connected layer FC7. At the time of its release, the VGG obtained expressive results in classification and localization problems.

\subsubsection{Inception}

The Inception network~\cite{szegedy2014}, created by Google, has an incarnation called GoogLeNet, which was designed with their focus to create a deep architecture that was computationally efficient. Consequentially, they got rid of the usual fully connected layer at the end of networks and introduced a module called Inception module.

The Inception module was used to try to create a local topology as if there was a network inside a network, stacked several times. Figure~\ref{fig:inception_module_naive} shows an Inception module, where each module applies several different types of filter operations in parallel, on top of the input coming from the previous layer.

These filters operations are 1$\times$1, 3$\times$3, 5$\times$5 convolutions and a 3$\times$3 pooling. In the end, they concatenate these filters output together and this output is fed to the next inception layer. These filter operations are computationally heavy, so they introduced bottlenecks layers into each inception module. The final result can be seen in Figure~\ref{fig:inception_module_bottle}, where a 1$\times$1 convolutional operation is performed before the 3$\times$3 and 5$\times$5 convolutions and right after the 3$\times$3 pooling. The introduction of the bottleneck keeps the features from growing too much between each Inception module.

\begin{figure}[!htb]
\centering
\subfloat[]{\includegraphics[width=12.0cm]{inception_module_naive}
\label{fig:inception_module_naive}} \\
\subfloat[]{\includegraphics[width=12.0cm]{inception_module_bottle}
\label{fig:inception_module_bottle}}
\caption{Inception module~\cite{szegedy2014}.}
\label{fig:inception_module}
\end{figure}

An overview of the network can be seen in Figure~\ref{fig:inception_over}. It has 22 layers, a small network at the beginning with a few convolutions and pooling, a classifier at the end and a few auxiliary classification outputs. The auxiliary outputs can also be used to do some classification with a shallower instance of the network.

\begin{figure}[!htb]
\centering
\includegraphics[width=17.0cm]{inception_over}
\caption{Inception~\cite{szegedy2014}.}
\label{fig:inception_over}
\end{figure}

\subsubsection{ResNet}

Residual Network (ResNet)~\cite{he2016} is a CNN architecture and one of the best solutions when dealing with image classification. ResNet became popular for proposing a solution that reduced much of the load to train deeper networks. It is composed of 152 layers and around 11 billion Floating Point Operations (FLOPs).

\begin{figure}[!htb]
\centering
\subfloat[]{\includegraphics[width=5.0cm]{resnet_1}
\label{fig:resnet_1}} \\
\subfloat[]{\includegraphics[width=16.5cm]{resnet_2}
\label{fig:resnet_2}}
\caption{ResNet~\cite{he2016}.}
\label{fig:resnet}
\end{figure}

The innovation proposed by this architecture is the concept of residual learning, in which instead of optimizing what is called the identity mapping, it is easier to optimize a residual mapping. Figure~\ref{fig:resnet_1} shows the identity map $X$ being recast into the residual map $F(X)$.

A concatenation of 34 layers is shown in Figure~\ref{fig:resnet_2}, where each layer has the following notation: (i) $n \times n$ is the filter dimension used on convolution operations, (ii) is the layer type, (iii) is the layer depth, and (iv) is the stride step, which if present means it is different from 1. The curved arrows in Figure~\ref{fig:resnet_2} are the shortcuts where the residual learning happens and dotted arrows represent a dimension reduction.

\subsection{Fall}

Since the definition of falling is subjective, a universal definition of such was not found in the literature. Some health organizations developed their own definition with some intersections, which could be used to define a general idea of a fall.

The definitions found in the literature are the following. The Joint Commission~\cite{jtc} definition is "[...] a fall may be described as an unintentional change in position coming to rest on the ground, floor, or onto the next lower surface (e.g. onto a bed, chair or bedside mat). [...]". The World Health Organization~\cite{who_fact} defines it as "[...] an event which results in a person coming to rest inadvertently on the ground or floor or other lower level. [...]". The Veterans Affairs National Center~\cite{va_fall} defines it as "Loss of upright position that results in landing on the floor, ground, or an object or furniture, or a sudden, uncontrolled, unintentional, non-purposeful, downward displacement of the body to the floor/ground or hitting another object such as a chair or stair [...]".

In this work, a fall will be defined as the intersection of these definitions as follows: an unintentional movement that results with the person landing on the ground.

\subsection{Optical Flow}

Optical flow~\cite{horn1981determining,barron1994performance} is a method for extracting a representation of the movements between two consecutive images. It is represented as a vector of direction and magnitude.

In the work by Lucas-Kanade et al.~\cite{lucas1981}, they proposed that a flow is the same for the local neighborhood of a selected pixel, making it possible to track such pixels based on their neighborhood. The method assumes that these pixels will not leave the frame, between each frame, and since it is a local method, uniform regions do not provide good pixels to be tracked.

\subsection{Saliency Map}

Saliency map~\cite{rudoy2013learning,li2002saliency} is a technique for generating a map of attention regions on images. In more recent approaches, these regions represent important aspects to classify a specific class. These important aspects are learned through deep networks using many examples of the class. Later on, when the model is given an image, it will look for the learned features and classify the image class. In a more classic approach, the saliency can be calculated through key points detection or the movement of these key points between frames.

\subsection{Skeleton Pose}

Skeleton pose~\cite{saggese2018learning,menier20063d} estimation techniques focus on searching for the human shape, either with depth images, body segmentation, or joint tracking. Currently, the state of the art of these techniques utilizes RGB images and a bottom-up approach to detect multiple person skeleton.

Usually, the skeleton estimation will detect a few key points corresponding to joints on the human body, from these joints the algorithm will try to reconstruct each segment by following clues given by the joint surrounding region. This technique is bound to mistakes, especially in scenarios involving rare poses or appearances, occlusions, overlapping parts, and false negatives.

\section{Related Work}
\label{sec:related}

In this section, we walk through some related work and their contribution to the theme. The works were clustered based on the kind of sensor they used to acquire their data. These clusters include wearable sensors, ambiance sensors, and vision sensors. Since vision sensors are the focus of this proposal they are further clustered into works with and without depth information.

\subsection{Wearable Sensors}

This approach relies on wearable sensors on the subjects' body to gather input data; gadgets like watches, accelerometers, gyroscopes, and smartphones. The major settle back of this approach is the resistance of the subjects towards wearing the equipment.

Among the three approaches discussed here, this is considered either too intrusive or raised many false alarms, as reported in Kukharenko et al.~\cite{kukharenko2017}. Zhao et al.~\cite{zhao2018} gathered data from a tri-axial gyroscope on the subject waist (Figures~\ref{fig:zhao_1} and~\ref{fig:zhao_2}) and used a decision tree to classify the signal. The experiments were performed on five random subjects and the following results were obtained: 99.53\% accuracy, 0.993 precision, 0.995 recall, and 0.994 F-measure.

Kumar et al.~\cite{kumar2018} used a sensor attached to the subjects' belt to gather data (Figure~\ref{fig:kumar}) and compared four methods to detect falls: thresholding, Support Vector Machines (SVM), K-nearest neighbor (KNN), and dynamic time warping. They experimented on six volunteers and obtained 93\% accuracy when using SVM. They argued that the wearable sensors were the best solution because they could monitor the subject at all times, especially against a camera blind spots.

Kukharenko et al.~\cite{kukharenko2017} obtained data from a wrist-worn device (Figure~\ref{fig:kukharenko}). Their solution revolves around threshold activation. The experiments were performed on volunteers and obtained 66\% accuracy.

\begin{figure}[!htb]
\centering
\subfloat[waist device~\cite{zhao2018}]{\includegraphics[width=0.30\textwidth]{zhao2018_1}
\label{fig:zhao_1}} \hspace*{0.3cm}
\subfloat[waist device~\cite{zhao2018}]{\includegraphics[width=0.30\textwidth]{zhao2018_2}
\label{fig:zhao_2}} \\[0.4cm]
\subfloat[belt device~\cite{kumar2018}]{\includegraphics[width=0.30\textwidth]{kumar2018}        \label{fig:kumar}} \hspace*{0.3cm}
\subfloat[wrist device~\cite{kukharenko2017}]{\includegraphics[width=0.30\textwidth]{kukharenko2017}
\label{fig:kukharenko}}
\caption{Wearable devices.}
\label{fig:wearable}
\end{figure}

\subsection{Ambiance Sensors}

This approach can be defined by the use of non-intrusive sensors that are not cameras. Their non-intrusiveness is an alternative to wearable sensors, and their regard towards privacy is an alternative to cameras. The main setback of this approach is the high rate of false positive (FP) accusations.

Khin et al.~\cite{khin2017} developed a triangulation system using two parallel arrays of sensors placed in a room. A human in the room would interfere with both arrays' signal and in a fall scenario. The pattern generated on both arrays could be used to detect the fall. No results were reported, but they experimented with a human getting in the room equipped with the sensors and a human falling in the room, on both cases the array reacted to the presence and movement of the human and according to the paper it was an evidence that it could be used to detect falls.

Zigel et al.~\cite{zigel2009method} use accelerometers and microphones to detect vibrations on the ground caused by falls. This information is segmented and fed to a quadratic classifier. They experimented by dropping a "Rescue Randy" (Figure~\ref{fig:zigel2009}) on the floor and measuring the vibrations captured by the sensors, and reported 97.5\% sensitivity and 98.6\% specificity.

\begin{figure}[!htb]
\centering
\subfloat[]{\includegraphics[width=0.3\textwidth]{zigel2009a}\label{fig:zigel2009a}}
\hspace*{0.3cm}
\subfloat[]{\includegraphics[width=0.3\textwidth]{zigel2009b}\label{fig:zigel2009b}}
\caption{"Rescue Randy"~\cite{zigel2009method}.}
\label{fig:zigel2009}
\end{figure}

\subsection{Vision Sensors}

This approach relies on images as input to perform on. Its main appeal is the corporal non-intrusiveness of the sensors, especially when compared with wearable ones. 

The sensors are cameras and can be subdivided into two smaller groups, single camera, and multiple cameras, both use 2D RGB images, however, the later has access to depth information. In some cases, there is only one camera but it is considered multiple cameras, because it is complemented by a sensor with depth information, as in the Microsoft Kinect.

\subsubsection{Multiple Cameras}

Shojaei-Hashemi et al.~\cite{shojaei-hashemi2018} used a Long Short-Term Memory (LSTM) neural network, on the skeleton obtained by a Microsoft Kinect camera. The skeleton is obtained via depth information, which sped-up the process to the point of skeleton estimation being done in real-time. They executed experiments in the NTU RGB+D Action Recognition Dataset, which contains 60 actions, including falls, and obtained 93.23\% precision and 96.12\% recall.

Mohd et al.~\cite{mohd2017} used joint detection from a Kinect camera, alongside with height, velocity, and acceleration of the human body, fed this information to an SVM to classify between several ADLs and falls. They experimented on three datasets, TST Fall Detection Dataset, URFD, and Fall Detection by Zhong Zhang, reporting results for each dataset in the same order, 90.91\%, 96.67\%, and 84.62\% accuracy, 81.82\%, 93.33\%, and 73.08\% sensitivity, and 100\%, 100\%, and 96.05\% specificity.

Nizam et al.~\cite{nizam2017} used background subtraction with joint tracking, obtained from a Kinect sensor. The system would raise a fall alarm if the body speed was beyond a threshold, and the joints were on the ground. They reported 93.94\% accuracy, 100\% sensitivity, and 91.3\% specificity on a privately created dataset.

Mousse et al.~\cite{mousse2017} used two cameras pointing to the same scene, and by extracting and crossing the foreground information from the two cameras they were able to extract features that would be used to detect a fall. They also tracked subjects under the camera. This information was fed to a threshold based decision system. They experimented with Multicam dataset and reported 95.8\% sensitivity, 100\% specificity, and 15.25 FPS.

\subsubsection{Single Camera}

Fan et al.~\cite{fan2018early} used the VGG16 architecture to score and monitor the completion degree of an event. The VGG was fed with dynamic images, a stack of frames representing features of the first stacked frame. They experimented on two datasets, URFD and Youtube Fall Dataset, and reported their results in terms of event completion. For URFD with 70\% fall fraction, they obtained 90\% specificity and sensitivity, with 30\% fall fraction they reported 52.1\% sensitivity and 70.1\% specificity, whereas for Youtube Fall Dataset with 70\% fall fraction they reported 50\% specificity.

Min et al.~\cite{min2018} used R-CNN fed with video frames to perform a scene analysis, which obtained spatial information between furniture and human, then an activity extractor classifier was employed. They experimented on three datasets (KTH dataset for human action classification, URFD dataset, and a privately created one), obtaining 95.5\% accuracy, 94.95\% recall, and 94.44\% precision, without specifying in which dataset.

Lie et al.~\cite{lie2018} extracted 2D skeletons, with a CNN, from the video frames and used an RNN with LSTM state cells to process skeleton temporal information. They experimented on a privately created dataset and reported an average of 88.9\% accuracy.

Li et al.~\cite{li2017fall} used a CNN similar to AlexNet, fed with each frame of the video on the UR Fall Detection Dataset. They reported achieving real-time classification between ADL and Falls and obtained 99.98\% accuracy, 100\% sensitivity, and 99.98\% specificity.

Tran et al.~\cite{tran2017} used SVM with multi-model features on a hybrid approach, using either a single camera or depth information based on availability, fed with a 2D skeleton and video frames. They experimented with a few datasets (two privately created, URFD and Le2i), obtaining 99.37\% accuracy, 96.77\% precision, 100\% sensitivity, and 99.23\% specificity on URFD dataset, as well as 97.90\% accuracy, 96\% precision, 97.95\% sensitivity, and 97.87\% specificity on FDD dataset.

Zerrouki et al.~\cite{zerrouki2018vision} segmented the human body from the background in each frame, and further segmented the human body into five partitions. The activity recognition was performed on the shape variation of these partitions, by feeding them to an AdaBoost algorithm. They experimented on two datasets (URFD and UMAFall), achieving 96.56\% accuracy.

Fan et al.~\cite{fan2018human} extracts the human body from the background, and fit an ellipse in the silhouette. Six shape features are obtained from the silhouette and fed to a slow feature function. The slow feature sequences generated are then used in an SVM to classify human action. They experimented on two datasets (Multicam and SDUFall), achieving 94\% accuracy on Multicam and 96.57\% accuracy on SDUFall.

Lee et al.~\cite{lee2005intelligent} used background subtraction combined with a connective component labeling to isolate the human shape silhouette, and determined their posture via silhouette perimeter, feret diameter, and the speed of the center of the silhouette. They experimented on a privately created dataset, reporting 77\% TP, 23\% FN, 5\% FP, and 95\% TN.
