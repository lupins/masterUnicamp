\chapter{Experimentos e Resultados}
\label{chap:experiment}

Neste capítulo, nós apresentamos as experimentações realizadas sobre o método proposto. Na seção seguinte, nós descrevemos os conjuntos de dados utilizados nos experimentos. Posteriormente, nós reportamos os resultados que cada arquitetura obteve nestas bases de dados. Ao final, os resultados são comparados com a literatura relacionada e suas contribuições para a detecção de quedas discutidas.

\section{Bases de Dados}
\label{sec:datasets}

Durante a revisão da literatura relacionada, várias bases de dados foram encontradas, porém, nem todas estão disponíveis publicamente, algumas referências para seus dados estão inativas ou os autores não responderam a nossa tentativa de contato. Dessa maneira, duas bases de dados relacionadas a quedas humanas foram selecionadas: (i) URFD~\cite{kwolek2014human} e (ii) FDD~\cite{charfi2013optimised}.

\subsubsection{URFD}

Publicada por Kwolek e Kepski~\cite{kwolek2014human}, a base de dados URFD (\textit{University of Rzeszow Fall Detection Dataset}) é composta de 70 sequências de vídeo, sendo 30 de quedas e 40 de atividades do dia-a-dia. Cada vídeo possui 30 quadros por segundo (FPS), com resolução de 640$\times$240 pixels e duração variada.

As sequências de queda foram gravadas com um acelerômetro e duas câmeras Microsoft Kinect, uma com visão horizontal da cena e uma com visão de cima para baixo, no teto. As atividades da vida diária foram gravadas com uma única câmera de visão horizontal e um acelerômetro. As informações do acelerômetro foram excluídas dos experimentos por extrapolarem o escopo do projeto. A Figura~\ref{fig:URFD} ilustra os cinco cenários de ADLs e um cenário de queda.

\begin{figure}[!htb]
\centering
\subfloat{\includegraphics[width=7.5cm]{figures/exp_urfd_fall}} \hspace*{0.3cm}
\subfloat{\includegraphics[width=7.5cm]{figures/exp_urfd_adl}}
\caption[Base de dados URFD]{Ambientes da base de dados URFD~\cite{kwolek2014human}. (a) ilustração dos cenários de quedas gravados pela câmera horizontal; (b) ilustração dos cinco ambientes onde as ADLs foram gravadas.}
\label{fig:URFD}
\end{figure}

A base de dados está anotada com as seguintes informações:

\begin{itemize}
\item Postura do sujeito (não deitado, deitado no chão e transição).
\item Razão entre altura e largura da caixa delimitadora.
\item Razão entre eixo máximo e mínimo.
\item Razão da ocupação do sujeito na caixa delimitadora.
\item Desvio padrão dos pixels para o centroide dos eixos $X$ e $Z$.
\item Razão entre altura do sujeito no quadro com a altura do sujeito em pé.
\item Altura do sujeito.
\item Distância do centro do sujeito ao chão.
\end{itemize}

\subsubsection{FDD}

O conjunto de dados FDD (\textit{Fall Detection Dataset}) foi publicado por Charfi et al.~\cite{charfi2013optimised} e contém 191 sequências de vídeo, com 143 sendo de quedas e 48 de atividades do dia-a-dia. Cada vídeo possui 25 FPS, com resolução de 320$\times$240 pixels e duração variada.

Todas as sequências foram gravadas com uma única câmera, em quatro ambientes diferentes: casa, copa, escritório e sala de aula, ilustrados na Figura~\ref{fig:FDD}. Além disso, a base apresenta três protocolos de experimentação: (i) em que treinamento e teste são criados com vídeos dos ambientes casa e copa, (ii) em que o treinamento é composto de vídeos da copa e o teste com vídeos do escritório e da sala de aula e (iii) em que o treinamento contém vídeos da copa, do escritório e da sala de aula e o teste contém vídeos de escritório e da sala de aula. Pelo fato da base URFD possuir somente um cenário onde as quedas foram gravados, e a FDD possuir vários, nós podemos afirmar que, por contraste, o conjunto URFD é um conjunto fácil e o conjunto FDD é um conjunto difícil, o que é refletido nos resultados dos experimentos.

\begin{figure}[!htb]
\centering
\subfloat{\includegraphics[width=7.5cm]{figures/exp_fdd_fall}} \hspace*{0.3cm}
\subfloat{\includegraphics[width=7.5cm]{figures/exp_fdd_adl}}
\caption[Base de dados FDD]{Ambientes da base de dados FDD~\cite{charfi2013optimised}. (a) ilustração dos vídeos contendo quedas; (b) ilustração dos vídeos de ADLs.}
\label{fig:FDD}
\end{figure}

A base de dados está anotada com as seguintes informações:

\begin{itemize}
\item Quadro inicial da queda.
\item Quadro final da queda.
\item Altura, largura e coordenadas do centro da caixa delimitadora de cada quadro.
\end{itemize}

\subsection{Métricas de Avaliação}

Neste trabalho, nós abordamos o problema da detecção de quedas como uma classificação binária, em que um classificador deve decidir se um quadro de vídeo corresponde a uma queda ou não. Para tal, as métricas escolhidas e suas respectivas equações foram as seguintes: (i) precisão (Equação~\ref{eq:precision}), (ii) sensibilidade (Equação~\ref{eq:sensitivity}), (iii) acurácia (Equação~\ref{eq:accuracy}) e (iv) acurácia balanceada (Equação~\ref{eq:b_accuracy}). Nas equações, os seguintes termos foram abreviados: TP (\textit{true positive}), FP (\textit{false positive}), TN (\textit{true negative}) e FN (\textit{false negative}).
\begin{align}
\textit{Precisão} &= \frac{\text{TP}}{\text{TP + FN}} \label{eq:precision} \\
\textit{Sensibilidade} &= \frac{\text{TN}}{\text{TN + FP}} \label{eq:sensitivity} \\
\textit{Acurácia} &= \frac{\text{TP + TN}}{\text{TP + TN + FP + FN}} \label{eq:accuracy} \\
\textit{Acurácia Balanceada} &= \frac{1}{\sum \hat{w_i}}\sum_{i}(\hat{y_{i}} = y_{i})\hat{w_i} \label{eq:b_accuracy}
\end{align}
\noindent em que
\begin{equation}
\hat{w_i} = \frac{w_i}{\sum_{j} 1(y_{j}=y_{i}) w_{j}}
\end{equation}

Essas métricas foram escolhidas pela necessidade de comparar nossos resultados com os encontrados na literatura que, em sua maioria, reportaram apenas: precisão, sensibilidade e acurácia. Como ambas as bases de dados são desbalanceadas, de maneira que a classe negativa possui mais do que o dobro de amostras do que a classe positiva (classe de quedas), nós selecionamos a acurácia balanceada para contrapor este fato, uma vez que ela é invariável ao desbalanceamento dos conjuntos de dados.

\subsection{Recursos Computacionais}

As arquiteturas propostas foram implementadas na linguagem de programação Python~\cite{rossum1995}, que foi escolhida devido a sua ampla disponibilidade de bibliotecas para aplicações de análise de imagens e aprendizado profundo. Mais especificamente, foram utilizadas as bibliotecas, foram utilizadas SciPy~\cite{scipy}, NumPy~\cite{oliphant2015}, OpenCV~\cite{opencv_library} e Keras~\cite{chollet2015}.

Os algoritmos de aprendizado profundo são conhecidos por serem computacionalmente intensivos. Seus treinamentos e experimentos requerem mais poder computacional do que um {\it notebook} convencional pode fornecer e, portanto, foram realizados na nuvem em uma máquina alugada da Amazon AWS, g2.2xlarge, com as seguintes especificações: 1x GPU nVidia GRID K520 (Kepler), 8x vCPUs e 15GB de memória RAM.

\section{Experimentos e Resultados}

A seguir, os experimentos realizados são apresentados. Os resultados foram separados de acordo com a arquitetura utilizada e reportados na ordem: VGG-16 e Inception 3D. Dentro da seção de cada arquitetura, as combinações de multicanais foram comparadas entre si. Posteriormente, para as combinações multicanais, nós também realizamos testes cruzados entre as bases de dados e, ao conhecimento dos autores, esta comparação é inédita entre os conjuntos de dados selecionados. Por fim, os melhores resultados foram comparados aos trabalhos da literatura relacionada. As bases de dados foram divididas nas proporções: 65\% para treinamento, 15\% para validação e 20\% para teste.

\subsubsection{Resultados para VGG-16}

As tabelas a seguir comparam as abordagens multicanais sobre a arquitetura VGG-16, com parâmetros de 500 épocas, empregando-se \textit{early stopping} com valor de paciência igual a 10, taxa de aprendizado de $10^{-4}$, \textit{mini-batches} de $2^{10}$, 50\% de \textit{dropout}, otimizador Adam, com treinamento para minimizar a função de perda da validação. Os parâmetros foram reutilizados de trabalhos semelhantes de classificação de eventos. Em ordem, os resultados foram reportados sobre a base URFD, seguidos dos resultados do conjunto FDD.

A Tabela~\ref{tab:vgg_urfd} exibe os resultados obtidos para a base URFD, na qual a combinação dos canais de fluxo óptico e ritmo visual obteve o melhor resultado. Em sua maioria, os melhores resultados foram obtidos dos canais de fluxo, ritmo e RGB, ao contrário dos canais contendo a pose.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[VGG-16 multicanais URFD]{Comparação VGG-16 dos multicanais em relação à base de dados URFD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
OF VR     & \textbf{1,00} & 0,98          & \textbf{0,98} & \textbf{0,96} \\
OF RGB VR & \textbf{1,00} & 0,97          & 0,97          & 0,94 \\
OF RGB    & 0,99          & 0,98          & 0,97          & 0,94 \\
RGB VR    & \textbf{1,00} & 0,90          & 0,90          & 0,92 \\
OF RGB SA & 0,99          & 0,98          & 0,97          & 0,91 \\
OF RGB PE & 0,99          & 0,98          & 0,97          & 0,90 \\
OF SA     & 0,99          & 0,98          & 0,97          & 0,90 \\
RGB SA    & 0,99          & 0,94          & 0,94          & 0,90 \\
SA VR     & 0,99          & 0,95          & 0,94          & 0,90 \\
OF PE     & 0,99          & \textbf{0,99} & \textbf{0,98} & 0,88 \\
VR PE     & 0,99          & 0,98          & 0,97          & 0,88 \\
RGB PE    & 0,99          & 0,97          & 0,96          & 0,87 \\
SA PE     & 0,99          & 0,97          & 0,96          & 0,87 \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:vgg_urfd}
\end{table}

A eficácia satisfatória do canal de fluxo vai de acordo com a hipótese de que a classificação de eventos é dependente de uma relação temporal entre os quadros e o mesmo ocorre com os canais espaciais, indicando que as informações temporal e espacial se complementam. Porém, tendo em vista a natureza do canal de pose, era esperado que a combinação entre pose e fluxo óptico gerasse bons resultados, enquanto nenhuma das combinações envolvendo a pose obteve mais de 90\% de acurácia balanceada. Esse resultado é discutido posteriormente, durante os testes cruzados.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[VGG-16 multicanais FDD]{Comparação VGG-16 dos multicanais em relação à base de dados FDD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
SA VR     & \textbf{1,00} & \textbf{0,97} & \textbf{0,98} & \textbf{0,95} \\
OF SA     & \textbf{1,00} & 0,96          & 0,96          & 0,94          \\
OF PE     & 0,99          & 0,95          & 0,95          & 0,92          \\
SA PE     & 0,99          & 0,89          & 0,89          & 0,92          \\
RGB SA    & 0,99          & 0,96          & 0,96          & 0,91          \\
OF VR     & 0,99          & 0,95          & 0,95          & 0,88          \\
OF RGB    & 0,99          & 0,94          & 0,94          & 0,88          \\
OF RGB PE & 0,99          & 0,92          & 0,92          & 0,88          \\
OF RGB SA & 0,99          & 0,91          & 0,91          & 0,87          \\
OF RGB VR & 0,99          & 0,90          & 0,89          & 0,87          \\
RGB PE    & 0,99          & 0,81          & 0,82          & 0,83          \\
RGB VR    & 0,99          & 0,84          & 0,83          & 0,80          \\
VR PE     & 0,99          & 0,78          & 0,78          & 0,78          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:vgg_fdd}
\end{table}

Os resultados relativos à base FDD são apresentados na Tabela~\ref{tab:vgg_fdd}. Nesta instância, o melhor resultado foi a combinação da saliência, que é espacial, e o ritmo visual, que é espaço-temporal. Nestes resultados, os canais de RGB sofreram uma queda brusca em eficácia, ao contrário dos canais de pose e saliência, que se encontraram entre os três melhores resultados. Apesar dos canais espaciais de saliência e pose terem ascendido entre os melhores resultados, há uma peculiaridade a ser notada: ambos os canais possuem pouca ou nenhuma informação do fundo, assim como os canais do melhor resultado, saliência e fluxo. Nós voltaremos a este ponto na discussão dos resultados cruzados.

Os testes cruzados, em que o treinamento ocorre em um conjunto de dados e o teste no outro, foram realizados em ordem: (i) treinamento na base URFD com teste na FDD e (ii) treinamento no conjunto FDD com teste na base URFD.

A Tabela~\ref{tab:2d_cross_urfd_fdd} é referente ao primeiro teste cruzado. Há uma queda significativa na acurácia balanceada, saltando de 95\% na Tabela~\ref{tab:vgg_fdd} para 63\%. Nós acreditamos que esta queda é um efeito colateral da qualidade das bases de dados, pois este resultado foi obtido do treino na base fácil (URFD). Nessa base, os vídeos de quedas foram gravados com diferentes atores, entretanto, no mesmo ambiente e com a câmera na mesma posição. Por contraste entre as bases, podemos afirmar que a URFD é fácil e a FDD é difícil. Ao treinar na base fácil e testar na base difícil, a maioria dos canais não conseguiu discriminar entre queda e não queda, obtendo 50\% de acurácia balanceada.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[VGG-16 cruzado URFD FDD]{Comparação VGG-16 dos multicanais em teste cruzado, com treinamento na base URFD e teste no conjunto FDD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
SA PE     & \textbf{0,96} & \textbf{1,00} & \textbf{0,96} & \textbf{0,63} \\
OF SA     & 0,95          & \textbf{1,00} & 0,95          & 0,55          \\
OF PE     & 0,95          & \textbf{1,00} & 0,95          & 0,54          \\
OF RGB PE & 0,95          & \textbf{1,00} & 0,95          & 0,51          \\
OF RGB VR & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF RGB SA & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF RGB    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF VR     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB PR    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB VR    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB SA    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
SA VR     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
VR PE     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:2d_cross_urfd_fdd}
\end{table}

Os melhores resultados são um reflexo da homogeneidade da base de dados, pois somente aqueles com pouco acesso ao fundo do quadro conseguiram operar em um novo cenário. A deficiência do conjunto de dados explica a completa inversão dos resultados da combinação de saliência com pose, obtendo o pior resultado na Tabela~\ref{tab:vgg_fdd}, em que a informação espacial era muito mais fácil de ser classificada e o melhor resultado no teste cruzado.

O segundo teste cruzado é reportado na Tabela~\ref{tab:2d_cross_fdd_urfd}. Novamente, houve uma queda da acurácia balanceada, de 96\% na Tabela~\ref{tab:vgg_urfd} para 78\%, que é esperada durante uma troca de contexto dos dados. A importância da informação temporal é mantida, tendo em vista a presença do fluxo óptico entre os melhores resultados. Entre os testes cruzados, houve um aumento da acurácia balanceada, o que pode ser explicado ao considerarmos que, nesta instância, a rede foi treinada na base difícil e testada na base fácil.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[VGG-16 cruzado FDD URFD]{Comparação VGG-16 dos multicanais em teste cruzado, com treinamento na base FDD e teste no conjunto URFD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
OF PE     & \textbf{0,98} & 0,89          & 0,88          & \textbf{0,78} \\
OF RGB PE & 0,97          & 0,99          & \textbf{0,96} & 0,68          \\
VR PE     & 0,97          & 0,94          & 0,91          & 0,68          \\
OF SA     & 0,95          & 0,99          & 0,95          & 0,54          \\
SA PE     & 0,95          & 0,94          & 0,90          & 0,54          \\
RGB PE    & 0,95          & \textbf{1,00} & 0,95          & 0,52          \\
OF RGB SA & 0,95          & \textbf{1,00} & 0,95          & 0,51          \\
OF RGB VR & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF RGB    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF VR     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB VR    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB SA    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
SA VR     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:2d_cross_fdd_urfd}
\end{table}

Em geral, houve uma melhora, vide a maioria das acurácias balanceadas acima de 50\%. Dentre os canais com os melhores resultados, ambos os testes cruzados são consistentes, pois as combinações sem acesso ao fundo do quadro continuam superando as opostas. Porém, alguns canais espaciais ascenderam nos resultados, que é o caso do RGB. Isso pode ser explicado pela heterogeneidade da base FDD, fazendo com o que treinamento focasse menos em aspectos do ambiente e generalizando melhor seu aprendizado.

\subsubsection{Resultados para Inception 3D}

As tabelas a seguir comparam as abordagens multicanais sobre a arquitetura Inception 3D, utilizando os seguintes parâmetros: 500 épocas, empregando-se \textit{early stopping} com valor de paciência igual a 10, taxa de aprendizado de $10^{-5}$, \textit{mini-batches} de tamanho 192, 50\% de \textit{dropout}, otimizador Adam, com treinamento para minimizar a função de perda da validação. Em ordem, os resultados foram reportados sobre a base URFD, seguidos dos resultados do conjunto FDD.

O resultados obtidos sobre a base URFD são exibidos na Tabela~\ref{tab:3d_urfd}, na qual a combinação dos canais de fluxo óptico e RGB obtiveram o melhor resultado. Considerando a natureza da arquitetura em si, a melhora na acurácia balanceada máxima era esperada, uma vez que a arquitetura 3D possui um relação temporal interna, subindo de 96\% na Tabela~\ref{tab:vgg_urfd} para 98\%.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[Inception 3D multicanais URFD]{Comparação 3D dos multicanais em relação à base de dados URFD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
OF RGB    & \textbf{1,00} & \textbf{1,00} & 0,97          & \textbf{0,98} \\
OF VR     & 0,99          & 0,96          & 0,95          & 0,97          \\
RGB VR    & 0,99          & 0,90          & 0,96          & 0,97          \\
OF RGB SA & \textbf{1,00} & 0,99          & 0,98          & 0,94          \\
OF RGB VR & 0,99          & 0,99          & \textbf{0,99} & 0,94          \\
OF RGB PE & 0,99          & 0,96          & 0,96          & 0,91          \\
OF SA     & 0,99          & 0,98          & 0,94          & 0,91          \\
SA VR     & 0,99          & 0,94          & 0,94          & 0,90          \\
RGB SA    & 0,99          & 0,95          & 0,96          & 0,89          \\
SA PE     & 0,99          & \textbf{1,00} & 0,91          & 0,89          \\
RGB PE    & 0,99          & 0,99          & 0,92          & 0,89          \\
VR PE     & 0,99          & 0,96          & 0,94          & 0,88          \\
OF PE     & 0,99          & 0,97          & 0,92          & 0,87          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:3d_urfd}
\end{table}

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[Inception 3D multicanais FDD]{Comparação 3D dos multicanais em relação à base de dados FDD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
OF SA     & 0,99          & 0,99          & 0,98          & \textbf{0,98} \\
SA VR     & \textbf{1,00} & 0,98          & 0,98          & 0,96          \\
SA PE     & \textbf{1,00} & \textbf{1,00} & 0,97          & 0,96          \\
RGB SA    & \textbf{1,00} & 0,99          & \textbf{0,99} & 0,95          \\
OF PE     & 0,99          & 0,96          & 0,97          & 0,93          \\
OF RGB VR & 0,99          & 0,95          & 0,95          & 0,91          \\
OF VR     & 0,99          & 0,93          & 0,94          & 0,91          \\
RGB VR    & 0,99          & 0,94          & 0,89          & 0,91          \\
OF RGB    & 0,99          & 0,91          & 0,99          & 0,90          \\
VR PE     & 0,99          & 0,89          & 0,91          & 0,88          \\
OF RGB PE & 0,99          & 0,84          & 0,93          & 0,87          \\
OF RGB SA & 0,99          & 0,85          & 0,97          & 0,86          \\
RGB PE    & 0,99          & 0,80          & 0,91          & 0,84          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:3d_fdd}
\end{table}

De maneira semelhante aos resultados da VGG-16, os canais temporais se encontram entre os melhores resultados. Como o treinamento e o teste desta instância foram realizados no mesmo conjunto de dados, não é surpresa que os canais espaciais obtiveram novamente uma boa eficácia, exceto novamente os canais envolvendo a pose.

A Tabela~\ref{tab:3d_fdd} apresenta os resultados referentes ao conjunto de dados FDD. A Inception 3D obteve uma melhora na acurácia balanceada máxima, de 95\% na Tabela~\ref{tab:vgg_fdd} para 98\%. A combinação dos canais fluxo óptico e saliência obtiveram o melhor resultado, mantendo a importância da relação temporal na classificação.

Um fato interessante a ser notado é o de que, ao contrário dos resultados (Tabela~\ref{tab:vgg_fdd}) da VGG-16, os canais aparentam estar mais distribuídos em sua eficácia. Nós acreditamos que esse fato é uma consequência combinada entre a base de dados ser mais heterogênea e pelo fato da arquitetura 3D definir uma relação temporal interna, sofrendo menor influência do fundo.

Os testes cruzados a seguir foram realizados na ordem: (i) treinamento na base URFD com teste na FDD (Tabela~\ref{tab:3d_cross_urfd_fdd}) e (ii) treinamento no conjunto FDD com teste na base URFD (Tabela~\ref{tab:3d_cross_fdd_urfd}). O primeiro teste obteve sua maior acurácia balanceada com o valor de 68\% e o segundo teste com o valor de 84\%. A maioria das combinações de canais obteve 50\%. Ambos os testes apresentaram um cenário semelhante aos encontrados nos testes cruzados da VGG-16, pois as maiores acurácias balanceadas sofreram uma queda em relação aos testes não cruzados, sendo que a rede obteve melhores resultados no conjunto URFD do que na base FDD. Além das similaridades, o fato da Inception 3D assimilar melhor as informações temporais com as espaciais, contribuiu para resultados superiores aos testes cruzados da VGG-16.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[Inception 3D cruzado URFD FDD]{Comparação 3D dos multicanais em teste cruzado, com treinamento na base URFD e teste no conjunto FDD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
OF PE     & \textbf{0,97} & \textbf{1,00} & \textbf{0,97} & \textbf{0,68} \\
OF SA     & 0,96          & \textbf{1,00} & 0,96          & 0,57          \\
SA PE     & 0,95          & \textbf{1,00} & 0,96          & 0,55          \\
OF RGB PE & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
OF RGB VR & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
OF RGB SA & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
OF RGB    & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
OF VR     & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
RGB PE    & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
RGB VR    & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
RGB SA    & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
SA VR     & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
VR PE     & 0,95          & \textbf{1,00} & 0,96          & 0,50          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:3d_cross_urfd_fdd}
\end{table}

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[Inception 3D cruzado FDD URFD]{Comparação 3D dos multicanais em teste cruzado, com treinamento na base FDD e teste no conjunto URFD.}
\begin{tabular}{lrrrr}
\toprule
Canais & Precisão & Sensibilidade & Acurácia & Acurácia Balanceada \\
\midrule
OF PE     & \textbf{0,97} & 0,90          & 0,89          & \textbf{0,84} \\
VR PE     & 0,96          & 0,92          & 0,90          & 0,75          \\
OF RGB PE & 0,96          & 0,99          & \textbf{0,97} & 0,72          \\
SA PE     & 0,96          & 0,93          & 0,95          & 0,60          \\
OF SA     & 0,95          & 0,99          & 0,91          & 0,54          \\
RGB PE    & 0,95          & 0,99          & 0,91          & 0,54          \\
OF RGB SA & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF RGB VR & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF RGB    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
OF VR     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB VR    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
RGB SA    & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
SA VR     & 0,95          & \textbf{1,00} & 0,95          & 0,50          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{OF: Fluxo óptico, VR: Ritmo visual, SA: Saliência, PE: Estimativa de pose. Os resultados estão em ordem decrescente de acurácia balanceada e os maiores de cada coluna estão destacados em negrito.}
\label{tab:3d_cross_fdd_urfd}
\end{table}

\subsubsection{Resultados Comparados com a Literatura Relacionada}

Nas tabelas a seguir, nós comparamos nossos melhores resultados com os encontrados durante a revisão bibliográfica. As métricas utilizadas foram as mesmas reportadas pelos autores: precisão, sensibilidade e acurácia. Os resultados referentes ao conjunto de dados URFD são reportados na Tabela~\ref{tab:urfd_ours_them}, enquanto os referentes à base de dados FDD são apresentados na Tabela~\ref{tab:fdd_ours_them}.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[Método proposto vs literatura - URFD]{Comparação dos nossos resultados com a literatura para o conjunto de dados URFD.}
\begin{tabular}{lrrr}
\toprule
Autores & Precisão & Sensibilidade & Acurácia \\
\midrule
Método proposto Inception 3D                    & 0,99          & \textbf{0,99} & \textbf{0,99} \\
Lu et al.~\cite{lu2018deep}                     & -             & -             & \textbf{0,99} \\
Método proposto VGG-16                          & \textbf{1,00} & 0,98          & 0,98          \\
Panahi e Ghods~\cite{panahi2018human}           & 0,97          & 0,97          & 0,97          \\
Zerrouki e Houacine~\cite{zerrouki2018combined} & -             & -             & 0,96          \\
Harrou et al.~\cite{harrou2017vision}           & -             & -             & 0,96          \\
Abobakr et al.~\cite{abobakr2017skeleton}       & \textbf{1,00} & 0,91          & 0,96          \\
Bhandari et al.~\cite{bhandari2017novel}        & 0,96          & -             & 0,95          \\
Kwolek e Kepski~\cite{kwolek2015improving}      & \textbf{1,00} & 0,92          & 0,95          \\
N\'u\~nez-Marcos et al.~\cite{nunez2017vision}  & \textbf{1,00} & 0,92          & 0,95          \\
Sase e Bhandari~\cite{sase2018human}            & 0,81          & -             & 0,90          \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{Os trabalhos que não reportaram alguns de seus resultados foram retratados com um hífen (-). Os resultados estão em ordem decrescente de acurácia e os maiores de cada coluna estão destacados em negrito.}
\label{tab:urfd_ours_them}
\end{table}

Nossa arquitetura Inception 3D superou ou se igualou aos trabalhos revisados. A arquitetura VGG-16 superou a maioria dos resultados encontrados, porém, ficou abaixo do trabalho de Lu et al.~\cite{lu2018deep}. Como citado no Capítulo~\ref{chap:background}, o método de Lu et al.~\cite{lu2018deep} utiliza a arquitetura de rede neural recorrente LSTM que, assim como a nossa solução, foi desenvolvida para lidar com a relação temporal entre os dados por meio de um mecanismo de memória incluso na rede.

\begin{table}[!htb]
\setlength{\tabcolsep}{3.0mm}
\centering
\caption[Método proposto vs literatura - FDD]{Comparação dos nossos resultados com a literatura para o conjunto de dados FDD.}
\begin{tabular}{lrrr}
\toprule
Autores & Precisão & Sensibilidade & Acurácia \\
\midrule
Método proposto Inception 3D                     & \textbf{1,00} & \textbf{0,99} & \textbf{0,99} \\
Lu et al.~\cite{lu2018deep}                      & -             & -             & \textbf{0,99} \\
Método proposto VGG-16                           & \textbf{1,00} & 0,98          & 0,98          \\
Sehairi et al.~\cite{sehairi2018elderly}         & -             & -             & 0,98          \\
Zerrouki e Houacine~\cite{zerrouki2018combined}  & -             & -             & 0,97          \\
Harrou et al.~\cite{harrou2017vision}            & -             & -             & 0,97          \\
N\'u\~nez-Marcos et al.~\cite{nunez2017vision}   & 0,99          & 0,97          & 0,97          \\
Charfi et al.~\cite{charfi2012definition}        & 0,98          & \textbf{0,99} & -             \\
\bottomrule
\end{tabular}
\\[10pt]
\caption*{Os trabalhos que não reportaram alguns de seus resultados foram retratados com um hífen (-). Os resultados estão em ordem decrescente de acurácia e os maiores de cada coluna estão destacados em negrito.}
\label{tab:fdd_ours_them}
\end{table}
