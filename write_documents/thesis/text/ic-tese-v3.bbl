\begin{thebibliography}{10}

\bibitem{barron1994performance}
John~L Barron, David~J Fleet, and Steven~S Beauchemin.
\newblock {Performance of Optical Flow Techniques}.
\newblock {\em International Journal of Computer Vision}, 12(1):43--77, 1994.

\bibitem{beauchemin1995computation}
Steven~S. Beauchemin and John~L. Barron.
\newblock {The Computation of Optical Flow}.
\newblock {\em {ACM Computing Surveys}}, 27(3):433--466, 1995.

\bibitem{opencv_library}
G.~Bradski.
\newblock {The OpenCV Library}.
\newblock {\em {Dr. Dobb's Journal of Software Tools}}, 2000.

\bibitem{cao2018efficient}
Feilong Cao, Yuehua Liu, and Dianhui Wang.
\newblock {Efficient Saliency Detection Using Convolutional Neural Networks
  with Feature Selection}.
\newblock {\em {Information Sciences}}, 456:34--49, 2018.

\bibitem{cao2016realtime}
Zhe Cao, Tomas Simon, Shih-En Wei, and Yaser Sheikh.
\newblock {Realtime Multi-Person 2D Pose Estimation Using Part Affinity
  Fields}.
\newblock In {\em {IEEE Conference on Computer Vision and Pattern
  Recognition}}, 2016.

\bibitem{carneiro2019multi}
S.~Carneiro, G.~Silva, G.~Leite, R.~Moreno, S.~Guimaraes, and H.~Pedrini.
\newblock {Multi-Stream Deep Convolutional Network Using High-Level Features
  Applied to Fall Detection in Video Sequences}.
\newblock In {\em 26th International Conference on Systems, Signals and Image
  Processing}, pages 293--298, 2019.

\bibitem{carneiro2019deep}
S.A. Carneiro, G.P. Silva, G.V. Leite, R.~Moreno, S.J.F. Guimaraes, and
  H.~Pedrini.
\newblock {Deep Convolutional Multi-Stream Network Detection System Applied to
  Fall Identification in Video Sequences}.
\newblock In {\em 15th International Conference on Machine Learning and Data
  Mining}, pages 681--695, 2019.

\bibitem{charfi2013optimised}
Imen Charfi, Johel Miteran, Julien Dubois, Mohamed Atri, and Rached Tourki.
\newblock {Optimized Spatio-Temporal Descriptors for Real-Time Fall Detection:
  Comparison of Support Vector Machine and Adaboost-based Classification}.
\newblock {\em Journal of Electronic Imaging}, 22(4):041106, 2013.

\bibitem{chollet2015}
Fran\c{c}ois Chollet et~al.
\newblock Keras, 2015.
\newblock \url{https://keras.io}.

\bibitem{deng2009imagenet}
Jia Deng, Wei Dong, Richard Socher, Li-Jia Li, Kai Li, and Li~Fei-Fei.
\newblock {Imagenet: A Large--Scale Hierarchical Image Database}.
\newblock In {\em {IEEE Conference on Computer Vision and Pattern
  Recognition}}, 2009.

\bibitem{deng2014}
Li~Deng, Dong Yu, et~al.
\newblock {Deep Learning: Methods and Applications}.
\newblock {\em {Foundations and Trends{\textregistered} in Signal Processing}},
  7(3--4):197--387, 2014.

\bibitem{fan2018human}
Kaibo Fan, Ping Wang, and Shuo Zhuang.
\newblock {Human Fall Detection Using Slow Feature Analysis}.
\newblock {\em {Multimedia Tools and Applications}}, 2018.

\bibitem{fan2018early}
Yaxiang Fan, Gongjian Wen, Deren Li, Shaohua Qiu, and Martin~D Levine.
\newblock {Early Event Detection based on Dynamic Images of Surveillance
  Videos}.
\newblock {\em Journal of Visual Communication and Image Representation},
  51:70--75, 2018.

\bibitem{farneback2003two}
Gunnar Farneb{\"a}ck.
\newblock {Two--Frame Motion Estimation Based on Polynomial Expansion}.
\newblock In {\em {Scandinavian Conference on Image Analysis}}, 2003.

\bibitem{gall2009motion}
Juergen Gall, Carsten Stoll, Edilson De~Aguiar, Christian Theobalt, Bodo
  Rosenhahn, and Hans-Peter Seidel.
\newblock {Motion Capture Using Joint Skeleton Tracking and Surface
  Estimation}.
\newblock In {\em {IEEE Conference on Computer Vision and Pattern
  Recognition}}, 2009.

\bibitem{goodale1992separate}
Melvyn~A Goodale and A~David Milner.
\newblock {Separate Visual Pathways for Perception and Action}.
\newblock {\em {Trends in Neurosciences}}, 15(1):20--25, 1992.

\bibitem{goodfellow2016}
Ian Goodfellow, Yoshua Bengio, Aaron Courville, and Yoshua Bengio.
\newblock {\em {Deep Learning}}.
\newblock {MIT Press}, 2016.

\bibitem{geron2017hands}
Aurélien Géron.
\newblock {\em {Hands--on Machine Learning with Scikit-Learn and TensorFlow:
  Concepts, Tools, and Techniques to Build Intelligent Systems}}.
\newblock O'Reilly, 2017.

\bibitem{he2016deep}
Kaiming He, Xiangyu Zhang, Shaoqing Ren, and Jian Sun.
\newblock {Deep Residual Learning for Image Recognition}.
\newblock In {\em {IEEE Conference on Computer Vision and Pattern
  Recognition}}, 2016.

\bibitem{who2007world}
David~L. Heymann, T.~Prentice, and L.~T. Reinders.
\newblock {\em {The World Health Report: A Safer Future: Global Public Health
  Security in the 21st Century}}.
\newblock {World Health Organization}, 2007.

\bibitem{horn1981determining}
Berthold~KP Horn and Brian~G Schunck.
\newblock {Determining Optical Flow}.
\newblock {\em Artificial Intelligence}, 17(1-3):185--203, 1981.

\bibitem{scipy}
Eric Jones, Travis Oliphant, and Pearu Peterson.
\newblock {SciPy: Open Source Scientific Tools for Python}, 2001--.
\newblock \url{http://www.scipy.org}.

\bibitem{khin2017development}
Oo~Oo Khin, Quang~Minh Ta, and Chien~Chern Cheah.
\newblock {Development of a Wireless Sensor Network for Human Fall Detection}.
\newblock In {\em International Conference on Real-Time Computing and
  Robotics}, pages 273--278. IEEE, 2017.

\bibitem{krizhevsky2012}
Alex Krizhevsky, Ilya Sutskever, and Geoffrey~E Hinton.
\newblock {Imagenet Classification with Deep Convolutional Neural Networks}.
\newblock In {\em Advances in Neural Information Processing Systems 25}, pages
  1097--1105, 2012.

\bibitem{kukharenko2017picking}
Illia Kukharenko and Volodymyr Romanenko.
\newblock {Picking a Human Fall Detection Algorithm for Wrist--Worn Electronic
  Device}.
\newblock In {\em IEEE First Ukraine Conference on Electrical and Computer
  Engineering}, pages 275--277, 2017.

\bibitem{kumar2018wearable}
Vaishna~S Kumar, Kavan~Gangadhar Acharya, B~Sandeep, T~Jayavignesh, and Ashvini
  Chaturvedi.
\newblock {Wearable Sensor--Based Human Fall Detection Wireless System}.
\newblock In {\em {Wireless Communication Networks and Internet of Things}},
  pages 217--234. Springer, 2018.

\bibitem{kwolek2014human}
Bogdan Kwolek and Michal Kepski.
\newblock {Human Fall Detection on Embedded Platform Using Depth Maps and
  Wireless Accelerometer}.
\newblock {\em {Computer Methods and Programs in Biomedicine}},
  117(3):489--501, 2014.

\bibitem{lecun1998gradient}
Yann LeCun, L{\'e}on Bottou, Yoshua Bengio, and Patrick Haffner.
\newblock {Gradient--Based Learning Applied to Document Recognition}.
\newblock {\em {Proceedings of the IEEE}}, 86(11):2278--2324, 1998.

\bibitem{lee2005intelligent}
Tracy Lee and Alex Mihailidis.
\newblock {An Intelligent Emergency Response System: Preliminary Development
  and Testing of Automated Fall Detection}.
\newblock {\em {Journal of Telemedicine and Telecare}}, 11(4):194--198, 2005.

\bibitem{li2017beyond}
Heyi Li, Klaus Mueller, and Xin Chen.
\newblock {Beyond Saliency: Understanding Convolutional Neural Networks from
  Saliency Prediction on Layer-Wise Relevance Propagation}.
\newblock {\em Computer Research Repository}, 2017.

\bibitem{li2017fall}
Xiaogang Li, Tiantian Pang, Weixiang Liu, and Tianfu Wang.
\newblock {Fall Detection for Elderly Person Care Using Convolutional Neural
  Networks}.
\newblock In {\em 10th International Congress on Image and Signal Processing,
  BioMedical Engineering and Informatics}, pages 1--6, 2017.

\bibitem{li2002saliency}
Zhaoping Li.
\newblock {A Saliency Map in Primary Visual Cortex}.
\newblock {\em Trends in Cognitive Sciences}, 6(1):9--16, 2002.

\bibitem{lie2018human}
Wen-Nung Lie, Anh~Tu Le, and Guan-Han Lin.
\newblock {Human Fall-Down Event Detection Based on 2D Skeletons and Deep
  Learning Approach}.
\newblock In {\em International Workshop on Advanced Image Technology}, 2018.

\bibitem{lucas1981iterative}
Bruce~D Lucas, Takeo Kanade, et~al.
\newblock {An Iterative Image Registration Technique with an Application to
  Stereo Vision}.
\newblock {\em {International Joint Conference on Artificial Inteligence}},
  1981.

\bibitem{lusardi2017determining}
Michelle~M Lusardi, Stacy Fritz, Addie Middleton, Leslie Allison, Mariana
  Wingood, Emma Phillips, et~al.
\newblock {Determining Risk of Falls in Community Dwelling Older Adults: A
  Systematic Review and Meta-Analysis Using Posttest Probability}.
\newblock {\em {Journal of Geriatric Physical Therapy}}, 40(1):1, 2017.

\bibitem{ma2014depth}
Xin Ma, Haibo Wang, Bingxia Xue, Mingang Zhou, Bing Ji, and Yibin Li.
\newblock {Depth-Based Human Fall Detection Via Shape Features and Improved
  Extreme Learning Machine}.
\newblock {\em Journal of Biomedical and Health Informatics}, 18(6):1915--1922,
  2014.

\bibitem{menier2006skeleton}
Clement Menier, Edmond Boyer, and Bruno Raffin.
\newblock {3D Skeleton-based Body Pose Recovery}.
\newblock In {\em 3rd International Symposium on 3D Data Processing,
  Visualization and Transmission}, pages 389--396. IEEE Computer Society, 2006.

\bibitem{min2018detection}
Weidong Min, Hao Cui, Hong Rao, Zhixun Li, and Leiyue Yao.
\newblock {Detection of Human Falls on Furniture Using Scene Analysis Based on
  Deep Learning and Activity Characteristics}.
\newblock {\em IEEE Access}, 6:9324--9335, 2018.

\bibitem{mohd2017optimized}
Mohd Norzali~Haji Mohd, Y~Nizam, S~Suhaila, and M~Mahadi~Abdul Jamil.
\newblock {An Optimized Low Computational Algorithm for Human Fall Detection
  from Depth Images Based on Support Vector Machine Classification}.
\newblock In {\em IEEE International Conference on Signal and Image Processing
  Applications}, 2017.

\bibitem{mousse2017percentage}
Mika{\"e}l~A Mousse, Cina Motamed, and Eugène~C Ezin.
\newblock {Percentage of Human--Occupied Areas for Fall Detection from Two
  Views}.
\newblock {\em The Visual Computer}, 33(12):1529--1540, 2017.

\bibitem{nizam2017human}
Yoosuf Nizam, Mohd Norzali~Haji Mohd, and M~Mahadi~Abdul Jamil.
\newblock {Human Fall Detection from Depth Images Using Position and Velocity
  of Subject}.
\newblock {\em Procedia Computer Science}, 105:131--137, 2017.

\bibitem{nunez2017vision}
A.~N{\'u}{\~n}ez-Marcos, G.~Azkune, and I.~Arganda-Carreras.
\newblock {Vision-based Fall Detection with Convolutional Neural Networks}.
\newblock {\em Wireless Communications and Mobile Computing}, 2017, 2017.

\bibitem{oliphant2015}
Travis~E Oliphant.
\newblock {\em {Guide to NumPy}}.
\newblock {USA: CreateS-pace Independent Publishing Platform}, USA, 2nd
  edition, 2015.

\bibitem{rudoy2013learning}
Dmitry Rudoy, Dan~B Goldman, Eli Shechtman, and Lihi Zelnik-Manor.
\newblock {Learning Video Saliency from Human Gaze using Candidate Selection}.
\newblock In {\em IEEE Conference on Computer Vision and Pattern Recognition},
  pages 1147--1154, 2013.

\bibitem{saggese2018learning}
Alessia Saggese, Nicola Strisciuglio, Mario Vento, and Nicolai Petkov.
\newblock {Learning Skeleton Representations for Human Action Recognition}.
\newblock {\em Pattern Recognition Letters}, 2018.

\bibitem{shojaei2018video}
Anahita Shojaei-Hashemi, Panos Nasiopoulos, James~J Little, and Mahsa~T
  Pourazad.
\newblock {Video--Based Human Fall Detection in Smart Homes Using Deep
  Learning}.
\newblock In {\em IEEE International Symposium on Circuits and Systems}, 2018.

\bibitem{simonyan2013deep}
Karen Simonyan, Andrea Vedaldi, and Andrew Zisserman.
\newblock {Deep Inside Convolutional Networks: Visualising Image Classification
  Models and Saliency Maps}.
\newblock {\em {Computer Research Repository}}, 2013.

\bibitem{simonyan2014two}
Karen Simonyan and Andrew Zisserman.
\newblock {Two--Stream Convolutional Networks for Action Recognition in
  Videos}.
\newblock In {\em {Advances in Neural Information Processing Systems 27}},
  2014.

\bibitem{simonyan2014very}
Karen Simonyan and Andrew Zisserman.
\newblock {Very Deep Convolutional Networks for Large--Scale Image
  Recognition}.
\newblock {\em International Conference on Machine Learning}, 2014.

\bibitem{szegedy2017inception}
Christian Szegedy, Sergey Ioffe, Vincent Vanhoucke, and Alexander~A Alemi.
\newblock {Inception--v4, Inception--Resnet and the Impact of Residual
  Connections on Learning}.
\newblock In {\em {Association for the Advancement of Artificial
  Intelligence}}, 2017.

\bibitem{szegedy2015going}
Christian Szegedy, Wei Liu, Yangqing Jia, Pierre Sermanet, Scott Reed, Dragomir
  Anguelov, et~al.
\newblock {Going Deeper with Convolutions}.
\newblock In {\em IEEE Conference on Computer Vision and Pattern Recognition},
  pages 1--9, 2015.

\bibitem{jtc}
{The Joint Commission}.
\newblock {Fall Reduction Program -- Definition of a Fall}, 2001.
\newblock
  \url{https://www.jointcommission.org/standards_information/jcfaqdetails.aspx?StandardsFAQId=1522&StandardsFAQChapterId=4&ProgramId=0&ChapterId=0&IsFeatured=False&IsNew=False&Keyword=},
  access 10--set--2018.

\bibitem{toshev2014deeppose}
Alexander Toshev and Christian Szegedy.
\newblock {Deepose: Human Pose Estimation via Deep Neural Networks}.
\newblock In {\em {IEEE Conference on Computer Vision and Pattern
  Recognition}}, 2014.

\bibitem{tran2017continuous}
Thanh-Hai Tran, Thi-Lan Le, Van-Nam Hoang, and Hai Vu.
\newblock {Continuous Detection of Human Fall Using Multimodal Features from
  Kinect Sensors in Scalable Environment}.
\newblock {\em Computer Methods and Programs in Biomedicine}, 146:151--165,
  2017.

\bibitem{va_fall}
{U.S. Department of Veterans Affairs}.
\newblock {Falls Policy Overview}.
\newblock
  \url{http://www.patientsafety.va.gov/docs/fallstoolkit14/05_falls_policy_overview_v5.docx},
  access 10--set--2018.

\bibitem{rossum1995}
Guido Van~Rossum and Fred~L Drake~Jr.
\newblock {\em {Python Reference Manual}}.
\newblock {Centrum voor Wiskunde en Informatica}, Amsterndam, 1995.

\bibitem{wang2015towards}
L.~Wang, Y.~Xiong, Z.~Wang, and Y.~Qiao.
\newblock {Towards Good Practices for very Deep Two-Stream Convnets}.
\newblock {\em arXiv preprint arXiv:1507.02159}, 2015.

\bibitem{who2011global}
{World Health Organization}.
\newblock {Global Health and Aging}, 2011.

\bibitem{who_fact}
{World Health Organization}.
\newblock {Fact Sheet Falls}, 2012.
\newblock \url{http://www.who.int/en/news-room/fact-sheets/detail/falls},
  access 10--set--2018.

\bibitem{who2015ageing}
{World Health Organization}.
\newblock {World Report on Ageing and Health}, 2015.

\bibitem{who2017reflecting}
{World Health Organization}.
\newblock {World Economic and Social Survey -- Reflecting on Seventy Years of
  Development Policy Analysis}, 2017.

\bibitem{zerrouki2018vision}
Nabil Zerrouki, Fouzi Harrou, Ying Sun, and Amrane Houacine.
\newblock {Vision-Based Human Action Classification Using Adaptive Boosting
  Algorithm}.
\newblock {\em IEEE Sensors Journal}, 18(12):5115--5121, 2018.

\bibitem{zhao2018recognition}
Shizhen Zhao, Wenfeng Li, Wenyu Niu, Raffaele Gravina, and Giancarlo Fortino.
\newblock {Recognition of Human Fall Events Based on Single Tri--Axial
  Gyroscope}.
\newblock In {\em IEEE 15th International Conference on Networking, Sensing and
  Control}, 2018.

\bibitem{zigel2009method}
Yaniv Zigel, Dima Litvak, and Israel Gannot.
\newblock {A Method for Automatic Fall Detection of Elderly People Using Floor
  Vibrations and Sound--Proof of Concept on Human Mimicking Doll Falls}.
\newblock {\em {IEEE Transactions on Biomedical Engineering}},
  56(12):2858--2867, 2009.

\end{thebibliography}
