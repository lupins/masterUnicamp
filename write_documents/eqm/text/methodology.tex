\chapter{Material and Methods}
\label{cap:methodology}

This chapter contains the description of the methodology that will be followed to develop a model for the fall detection in video sequences, the datasets used to validate our architecture, as well as the metrics used to evaluate the recognition process.

\section{Methodology}
\label{sec:method}

With the end goal of detecting human fall scenarios, using only RGB videos, the proposed methodology is composed of two stages, training and testing, illustrated in Figures~\ref{fig:methodology_training} and~\ref{fig:methodology_testing}. This methodology took as inspiration the works of Goodale et al.~\cite{goodale1992} and Simonyan et al.~\cite{simonyan2014two}. The first approach establishes a hypothesis that the human visual cortex consists of two parts that focus on different aspects of vision, whereas the second work, inspired by this statement, experimented with a two-stream network, that resulted in an improved performance.

\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{methodology_training}
\caption{Main components of the training stage.}
\label{fig:methodology_training}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{methodology_testing}
\caption{Main components of the testing stage.}
\label{fig:methodology_testing}
\end{figure}

\subsection{Training}

The training phase (Figure~\ref{fig:methodology_training}) consists of fetching a percentage of the selected datasets, highlighted in the yellow block in Figure~\ref{fig:methodology_training}, and feeding it to the network, so it can "learn" features on top of it. This learning can be done in raw RGB data, but previous work has shown that some pre-processing techniques can be employed, before being fed into the network for better results.

Based on the results obtained by Simonyan et al.~\cite{simonyan2014two} and Wang et al.~\cite{wang2015}, this proposal opted to experiment with a three-stream network. Each stream is fed with the output of a pre-processing technique.

These techniques, shown in the blue block in Figure~\ref{fig:methodology_training}, were selected to cover as many aspects of a human fall as possible in a video. The first is the widely used optical flow, the second is a saliency map, and the third one is human-skeleton tracking. They will be detailed in the following sections. It is important to note that as the experiments will be done, other techniques might be tested as well.

\subsection{Pre-Processing}

The following subsections will describe in more detail each of the pre-processing techniques proposed in the methodology, explaining their importance and how they are planned to be deployed in the solution.

\subsubsection{Optical Flow}

A fall in the context of videos can be an event that occurs throughout a few sequential frames and in any environment. A naive approach to detecting it would be to look for a fall in each frame individually, ignoring the temporal relationship between the current frame, its predecessor, and its successor. In fact, it takes only a few frames to characterize a fall event, and the optical flow is one of the tools that take this temporal characteristic into account.

The optical flow is one of the pre-processing tools chosen to account for the temporal dependence of a fall event, representing the human movement effectively and ignoring any static environmental changes in the background. Initially, this proposal's solution will be tested with the implementation of dense optical flow proposed by Farneback et al.~\cite{farneback2003}, which is already implemented in the OpenCV~\cite{opencv_library} library. Other optical flow algorithms will also be investigated, if whether or not they influence the end result, such as the one proposed by Beauchemin et al.~\cite{beauchemin1995}.

Since the optical flow represents only the relationship between two frames, therefore, to characterize a fall event, a longer period must be taken into account. To address this limitation, a set of optical flow images will be stacked and fed to the CNN, which will extract an array of features $F \in \mathbb{R}^{w*h*s}$, where $w$ and $h$ are the width and height of the frames and $s$ is the stack size. As suggested by Wang et al.~\cite{wang2015}, the stack will be 10 optical flow images, i.e. 10 directional images and 10 magnitude images. Throughout the process, a particular input video will be processed in a sliding window, with step 1 and size 10.

\subsubsection{Saliency Map}

Saliency map has been used to create a sense of attention to a particular aspect of a given image. Usually, the solution will learn features that describe an entire class, for instance, the fur texture in dogs will generate a higher saliency pattern that will react to that kind of region. Later, when presented with a dog photo, the now trained solution will look for those saliency peaks, and they will be part of the "attention" regions.

This proposal assumes that a fall event will generate a specific pattern of activation around the human body, and by feeding this behavior to a CNN, it can be used as a component to detect falls. Initial tests will be carried out with the implementation embedded in the Keras~\cite{chollet2015} library. The implemented approach is similar to the saliency map previously explained in Section~\ref{sec:concept} and is based on the work by Simonyan et al.~\cite{simonyan2013deep}. Some other solutions will also be tested, such as Li et al.~\cite{li2017beyond} and Cao et al.~\cite{cao2018}.

\subsubsection{Skeleton Pose}

Skeleton pose estimation using only RGB data had a quick and accurate result in the work by Cao et al.~\cite{cao2016realtime}. They were able to estimate the skeleton position of several individuals in real time using a bottom-up approach to the problem.

Experiments will be done feeding one of the CNN streams with the human skeleton information. Since it is the only stream that will have clearer information about the human body, it is expected to be a valuable component in the final solution.

The initial experimentation will be carried out with the implementation by Cao et al.~\cite{cao2016realtime}, which reported some failures cases such as, rare pose, rare appearance, occlusion, overlapping parts, and false negatives. These cases must be evaluated in a fall scenario, especially since after a fall the human body may end up occluded by furniture or in a position where the body occlude some limbs. Furthermore, other implementation will also be tested, such as Gall et al.~\cite{gall2009motion} and Toshev et al.~\cite{toshev2014deeppose}.

\subsection{CNN}

A fall event occurs over a period of time and, therefore, temporal information is important to the solution. Because of this, the use of Convolutional Neural Networks was evaluated, which works by applying convolution operations in small segments of a given video.

Initially, this work will explore some CNN architectures, shown in the green block in Figure~\ref{fig:methodology_training}, such as Inception~\cite{szegedy2014}, VGG16~\cite{simonyan2014very}, and ResNet~\cite{he2016}. These architectures showed promising results in previous work and VGG16, in particular, was used by Núñez-Marcos et al.~\cite{nunezmarcos2017} to detect human fall. Other architectures will also be investigated as the work progresses through the testing phase.

\subsection{Junction}

The three pre-processing methods proposed here were chosen for their performance and results in the reviewed literature, however, they have been applied as stand-alone solutions. Some works as Wang et al.~\cite{wang2015} and Simonyan et al.~\cite{simonyan2014two} highlight the performance boost when these methods had their results joined. These works paved the motivation of this proposal to junction the previous methods in a three-stream manner.

In our methodology, the CNN phase will output three heterogeneous results, representing the three selected approaches. By combining these approaches, shown in the pink block in Figure~\ref{fig:methodology_training}, means that each output will influence the final result proportionally to how effective it is for the end goal. It is possible that the chosen set of streams may not be effective in detecting falls, pushing more focus to investigate other methods of doing so.

This proportionality can be abstracted as a weight on a weighted mean. The reviewed literature has some methods for determining these weights, such as a simple mean, intuition estimates, and some automatic ways such as deploying an SVM to test which set of weights produces the best result. Initially, the junction (fusion) will be tested using an SVM to determine the weight of each stream and then compare it to some other methods previously mentioned.

\subsection{Testing}

The testing phase (Figure~\ref{fig:methodology_testing}) has a structure similar to that of training, as it divides the video into the same three pre-processing streams, explained in the training phase, shown in the blue block in Figure~\ref{fig:methodology_testing}, and feeds the data into three CNNs.

The training and testing phases diverge in the CNN step, shown in the green block in Figure~\ref{fig:methodology_testing}. During the training, each CNN stream would use a certain video component, optical flow, saliency map, or skeleton pose, to update their weights and learn what constitutes a fall. In the testing phase, the resulting model obtained from training is employed, shown in the purple block in Figure~\ref{fig:methodology_training} and the purple block in Figure~\ref{fig:methodology_testing} and the weights are no longer updated. The CNN, now a classifier, will output if whether the video has or not a fall.

\section{Materials}
\label{sec:materials}

This section describes more detail on datasets and computational resources that will be used to train and validate the proposed solution and how the results will be evaluated.

\subsection{Datasets}

In order to allow comparison to other methods available in the literature, three  known datasets are initially considered: UR Fall Dataset (URFD)~\cite{kepski2014}, Multiple Cameras Fall Dataset (Multicam)~\cite{auvinet2010} and the Fall Detection Dataset (FDD)~\cite{charfi2013}.

The URFD dataset contains 70 sequences, 30 falls and 40 activities of daily living. Each video has 30 FPS, a resolution of 640$\times$240 pixels, and various lengths. The fall sequences were recorded with two Microsoft Kinect cameras and an accelerometer attached to the subject. Figure~\ref{fig:urfd} shows some snapshots from the dataset. The activities of daily living (ADLs) were recorded with a single camera and an accelerometer.

\begin{figure}[!htb]
\centering
\subfloat[side view, pre-fall]{\includegraphics[width=0.4\textwidth]{urfd_1} \label{fig:urfd_1}} \hspace*{0.1cm}
\subfloat[side view, fall]{\includegraphics[width=0.4\textwidth]{urfd_2} \label{fig:urfd_2}} \\
\subfloat[top view, pre-fall]{\includegraphics[width=0.4\textwidth]{urfd_3} \label{fig:urfd_3}} \hspace*{0.1cm}
\subfloat[top view, fall]{\includegraphics[width=0.4\textwidth]{urfd_4} \label{fig:urfd_4}}
\caption{Depth and RGB images from URFD dataset~\cite{kepski2014}.}
\label{fig:urfd}
\end{figure}

The Multicam dataset has 24 scenarios split into 22 scenarios with falls and two scenarios without falls. Each video was recorded from 8 different angles using IP cameras. They have 120 frames per second (FPS), a resolution of 720$\times$480 pixels, and various lengths, Figure~\ref{fig:multicam} shows some snapshots from the dataset. They are also annotated with 9 categories of activities, fall is one of them, and their beginning and end frame.

\begin{figure}[!htb]
\centering
\subfloat[camera 1, pre fall]{\includegraphics[width=0.3\textwidth]{multicam_1}
\label{fig:multicam_1}}
\subfloat[camera 1, fall]{\includegraphics[width=0.3\textwidth]{multicam_2}
\label{fig:multicam_2}} \\
\subfloat[camera 4, pre fall]{\includegraphics[width=0.3\textwidth]{multicam_3}
\label{fig:multicam_3}} 
\subfloat[camera 4, fall]{\includegraphics[width=0.3\textwidth]{multicam_4}
\label{fig:multicam_4}}
\caption{Cameras 1 and 4 point of view from Multicam dataset~\cite{auvinet2010}.}
\label{fig:multicam}
\end{figure}

The FDD dataset contains 191 videos recorded with a single camera in four different environments: home, coffee room, office and lecture room; 143 of the 191 videos contains falls and 48 are of activities of daily living. Each video has 25 FPS, a fixed resolution of 320$\times$240, and various lengths. They are also annotated with the frame at the beginning and end of the fall, and the bounding box around the human body. The dataset has three experimental protocols: (i) P1, where the training and testing subsets were created with videos from the locations "Home" and "Coffee", (ii) P2, which constructed the training subset with videos from "Coffee Room" and the test subset with videos from "Office" and "Lecture Room", and (iii) P3, which built the training subset with videos from "Coffee Room" and some videos without any falls from the subsets "Office" and "Lecture Room", whereas the testing was built with videos from "Office" and "Lecture Room" subsets. It also includes variable illumination, occlusion, textured and cluttered background.

\begin{figure}[!htb]
\centering
\subfloat[office, pre sleeping]{\includegraphics[width=0.3\textwidth]{fdd_1}
\label{fig:fdd_1}}
\subfloat[office, sleeping]{\includegraphics[width=0.3\textwidth]{fdd_2}
\label{fig:fdd_2}} \\
\subfloat[office, pre fall]{\includegraphics[width=0.3\textwidth]{fdd_3}
\label{fig:fdd_3}}
\subfloat[office, fall]{\includegraphics[width=0.3\textwidth]{fdd_4}
\label{fig:fdd_4}}
\caption{Office samples from FDD dataset~\cite{charfi2013}.}
\label{fig:fdd}
\end{figure}

\subsection{Computational Resources}

The proposed methodology will be implemented in Python programming language~\cite{rossum1995}, due to its widespread use in scientific applications. Python nowadays is filled with libraries prepared to deal with Machine/Deep Learning algorithms, such as SciPy~\cite{scipy}, NumPy~\cite{oliphant2015}, Keras~\cite{chollet2015}, Tensorflow~\cite{abadi2016} and OpenCV~\cite{opencv_library}.

Deep learning algorithms are known for being computationally intensive. The experiments will require more computational power than a conventional working notebook can provide and will be conducted in the cloud, using an Amazon AWS rented machine, g2.2xlarge, with the following specifications: 1 GPU nVidia GRID K520(Kepler), 8 vCPUs and 15GB of RAM memory.

\section {Evaluation Metrics}

Fall detection can be addressed as a binary classification problem since the classifier must decide whether a particular frame sequence has a fall event or not. To measure the performance of such classifier, a set of quantitative metrics will be measured based on true positive (TP), true negative (TN), false positive (FP) and false negative (FN) rates.

Recall or true positive rate measures how good the solution is at predicting a fall event. Its value is derived from Equation~\ref{eq:recall}.
\begin{equation}
\label{eq:recall}
\textit{Recall} = \frac{TP}{TP + FN}
\end{equation}

Specificity or true negative rate measures how good the solution is at not raising false alarms over a fall event. Its value is derived from Equation~\ref{eq:specificity}.
\begin{equation}
\label{eq:specificity}
\textit{Specificity} = \frac{TN}{TN + FP}
\end{equation}

Accuracy will be calculated for the sake of comparison with other existing work. Its value is derived from Equation~\ref{eq:accuracy}.
\begin{equation}
\label{eq:accuracy}
\textit{Accuracy} = \frac{TP + TN}{TP + TN + FP + FN}
\end{equation}

Recall and specificity are not biased by unbalanced class distribution, which is relevant in fall detection, since most datasets present a greater number of events without falling than actual fall ones. To account for this class distribution imbalance, another metric will be implemented, the F2 measure, which weights the FN, since it is more important to detect a fall than not to generate false alarms, F2 derives from Equation~\ref{eq:f_measure}.
\begin{equation}
\label{eq:f_measure}
F2 = (1 + 2^2) * \frac{\textit{Precision} * \textit{Recall}}{(2^2 * \textit{Precision}) + \textit{Recall}}
\end{equation}
