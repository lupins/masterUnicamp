\chapter{Metodologia}
\label{chap:method}

Neste capítulo, descrevemos dois métodos multicanais que foram propostos e utilizados para a detecção de quedas em vídeos neste trabalho: (i) método 2D utilizando a arquitetura convolucional VGG-16 e (ii) um método 3D utilizando a arquitetura Inception 3D.

A Figura~\ref{fig:method_overview} exibe uma visão geral dos dois métodos propostos que se baseiam na hipótese levantada por Goodale e Milner~\cite{goodale1992separate}, na qual o córtex visual humano é composto por duas partes que focam em processar diferentes aspectos da visão. Essa mesma hipótese inspirou Simonyan e Zisserman~\cite{simonyan2014two} a testar redes neurais com vários canais de informação que simulassem o córtex visual. Apesar da utilização de multicanais ter sido inspirada no córtex visual humano, as características extraídas quebram a analogia e representam um outro espaço de informações.

\begin{figure}[!htb]
\centering
\includegraphics[width=13.7cm]{figures/method_overview}
\caption[Diagrama geral da metodologia]{Diagrama geral para ambos os métodos propostos, ilustrando as fases de treinamento e teste.}
\label{fig:method_overview}
\end{figure}

\section{Pré-Processamento}

Na literatura relacionada ao aprendizado de máquina profundo, o conhecimento sobre a influência positiva de alguns passos de pré-processamento sobre a eficácia das redes é ubiquamente presente. Dessa maneira, alguns processamentos foram selecionados de forma a melhor atenderem à tarefa em questão.

Neste trabalho, o pré-processamento, representado pelo bloco em cor verde na Figura~\ref{fig:method_overview}, consiste na extração de características que possam capturar os vários aspectos de uma queda e na aplicação de técnicas de aumentação de dados (\textit{data augmentation}).

\subsection{Extração de Características}

Neste trabalho, as características de estimação de pose, ritmo visual, saliência e fluxo óptico, apresentadas no Capítulo~\ref{chap:background}, foram utilizadas e obtidas das formas a seguir. A estimação de pose foi extraída por uma abordagem \textit{bottom-up}, com o algoritmo OpenPose de Cao et al.~\cite{cao2017realtime}, o algoritmo está publicamente disponível e apresentou boa performace ao estimar a pose de vários indivíduos no quadro. Os quadros extraídos foram alimentados à rede um por vez e os resultados são obtidos quadro-a-quadro (Figura~\ref{fig:ex_pose}). A escolha desta característica se deu pela observação empírica de que a detecção de quedas pode se beneficiar com informações da pose do idoso. Para o ritmo visual, um algoritmo foi implementado tal que cada vídeo possui somente um ritmo visual. Esse quadro de ritmo foi alimentado repetidas vezes para a rede e emparelhado com os quadros de outras características (Figura~\ref{fig:ex_rhythm}). A escolha do ritmo vem do fato dele comprimir ao mesmo tempo informações temporais e espaciais, que supomos ser essenciais para detecção das quedas. O mapa de saliência foi obtido com a utilização da técnica de \textit{SmoothGrad}, proposta e implementada por Smilkov et al.~\cite{smilkov2017smoothgrad}, que atua sobre uma técnica previamente existente de Sundararajan et al.~\cite{sundararajan2017axiomatic}. O algoritmo foi escolhido pela forma como foi treinado, os autores treinaram a rede que extraí a saliência em uma base com vídeos egocêntricos, visão da primeira pessoa, de forma que a rede ignorasse a plano de fundo dos quadros, similarmente, os eventos que quedas acontecem destacados do plano de fundo, dessa forma o mapa de saliência mantém informações espaciais do~\textit{foreground} da cena. Os quadros de saliência foram alimentados à rede de forma emparelhada com os outros canais (Figura~\ref{fig:ex_saliency}).

\begin{figure}[!htb]
\centering
\includegraphics[width=12.0cm]{figures/method_sliding_window}
\caption[Janela deslizante]{Ilustração da janela deslizante e seu movimento entre os quadros de vídeo.}
\label{fig:sliding_window}
\end{figure}

A extração do fluxo óptico foi realizada com o algoritmo proposto por Farneb\"ack~\cite{farneback2003two}, que descreve o fluxo óptico denso (Figura~\ref{fig:ex_flow}). O algoritmo é publicamente disponível na biblioteca OpenCV e está presente em vários outros trabalhos da literatura que extraíram o fluxo óptico. A assumpção de que a classificação de eventos em vídeos está diretamente relacionada com a característica temporal das ações, nos motivou a escolher o fluxo óptico para descrever a relação temporal entre os quadros. Como um evento de queda acontece no decorrer de vários quadros e o fluxo representa somente a relação entre dois quadros, nós utilizamos uma abordagem de janela deslizante, sugerida por Wang et al.~\cite{wang2015towards}. A janela deslizante alimenta a rede com uma pilha de dez quadros de fluxo óptico. A primeira pilha contém quadros de 0 ao 9 e a segunda pilha quadros de 1 ao 10, e assim por diante com \textit{stride} igual a 1 (Figura~\ref{fig:sliding_window}). Dessa forma, cada vídeo possui $N - 10 + 1$ pilhas, assumindo $N$ como o número de quadros de um vídeo, sendo que os últimos nove quadros não contribuem na avaliação. O resultado de cada pilha foi associado ao primeiro quadro da mesma. Dessa forma, ela pode ser emparelhada com os outros canais da rede.

\subsection{Aumentação de Dados}

Técnicas de aumentação de dados (\textit{data augmentation}) foram utilizadas, quando aplicáveis, durante o pré-processamento da fase de treinamento. Os seguintes processos de aumentação foram empregados: espelhamento sobre o eixo vertical, transformação de perspectiva, corte e adição de bordas espelhadas, adição dos valores -20 e 20 aos pixels, adição dos valores -15 e 15 à matiz e à saturação.

Somente o canal de RGB ({\it Red-Green-Blue}) passou pelo processo, pois os outros canais sofreriam de forma negativa. Por exemplo, a informação do fluxo óptico depende estritamente da relação entre quadros e sua magnitude é expressa pelo brilho do pixel, um espelhamento quebraria a continuidade entre quadros e uma adição distorceria a magnitude do vetor.

\section{Treinamento}

Em função da pequena quantidade de dados disponíveis para nossa experimentação, a etapa de treinamento do método proposto necessita da utilização da técnica de transferência de aprendizado, descrita no Capítulo~\ref{chap:background}. A técnica em questão foi utilizada de forma semelhante tanto no método para a arquitetura VGG-16 quanto para a arquitetura Inception 3D.

Para o treinamento da Inception 3D, os pesos foram transferidos da base de dados ImageNet~\cite{deng2009imagenet}. A partir deste ponto, a rede foi treinada sem congelar nenhuma de suas camadas. Na arquitetura VGG-16, os pesos foram novamente adquiridos da base ImageNet~\cite{deng2009imagenet}, porém, somente os pesos das primeiras 14 camadas da rede foram mantidos. A partir deste ponto, essas camadas foram novamente treinadas na base UCF101~\cite{soomro2012ucf101} e seus pesos congelados, de forma que a etapa de treinamento ocorresse somente nas duas camadas totalmente conexas finais. O processo de transferência para as primeiras 14 camadas e o congelamento delas está ilustrado na Figura~\ref{fig:transfer_pre14}.

A escolha dos conjuntos de dados para a transferência do aprendizado foi baseada na qualidade do conjunto e na disponibilidade do mesmo. Em especial, no caso da base UCF101, a escolha se deu pelo fato de ela conter informações de vídeo sobre ações humanas e a transferência dessas informações poder impactar positivamente para a classificação de quedas.

\begin{figure}[!htb]
\centering
\includegraphics[width=14.0cm]{figures/method_transfer} 
\caption[Transferência de aprendizado]{Processo de transferência de aprendizado. Da esquerda para direita, o modelo inicia sem nenhum treinamento, a seguir ele é treinado na base ImageNet~\cite{deng2009imagenet}, e posteriormente ele é treinado no conjunto  UCF101~\cite{soomro2012ucf101}. A direita, ilustração do congelamento das primeiras camadas da VGG-16, representadas pelos círculos de cor azul, bem como o treinamento realizado nas duas camadas finais, representadas pela cor verde.}
\label{fig:transfer_pre14}
\end{figure}

Após a transferência de aprendizado, ambas as arquiteturas foram alimentadas com as características extraídas. Cada canal possui uma rede dedicada a aprender suas características e o número de canais simultâneos varia de dois a três, como ilustrado na Figura~\ref{fig:method_overview}. No caso da arquitetura VGG-16, somente as duas camadas finais foram treinadas, como explicado anteriormente e ilustrado na direita da Figura~\ref{fig:transfer_pre14}. No caso da arquitetura Inception 3D, toda a rede foi treinada na nova base de dados. Os pesos de cada base de dados, respectivos a cada canal, foram então salvos para serem reutilizados na fase de teste.

% \begin{figure}[!htb]
% \centering
% \includegraphics[width=12.0cm]{figures/method_freezing}
% \caption[Congelamento das camadas da VGG-16]{Ilustração do congelamento das primeiras camadas da VGG-16, representadas pelos círculos de cor azul, bem como o treinamento realizado nas duas camadas finais, representadas pela cor verde.}
% \label{fig:transfer_pos14}
% \end{figure}

\section{Teste}

Alguns trabalhos da literatura revisada~\cite{torres2018detection, cao2017realtime} utilizaram as características pré-selecionadas por esta metodologia para detectar ações humanas, entretanto, elas foram utilizadas de forma isolada. Estes trabalhos, juntamente com o trabalho de Simonyan et al.~\cite{simonyan2014two}, pavimentaram nossa motivação na proposição de uma metodologia que unisse, de alguma forma, estas características, agindo como um ator ponderador dos canais da rede. Essa união pode ser realizada de diversas maneiras, desde uma simples média entre os canais, passando por uma média ponderada, até alguns métodos automáticos, por exemplo, a forma utilizada nesta metodologia: a aplicação de SVM para classificar os resultados dos canais da rede.

A fase de teste, ilustrada na Figura~\ref{fig:method_overview}, utiliza como entrada os mesmos canais de características da fase de treinamento. Porém, as semelhanças se limitam a isso: ambas as etapas de aumentação de dados e de transferência de aprendizado não foram executadas nesta fase. Os pesos obtidos pelo treinamento foram carregados e as arquiteturas de CNN os utilizam para classificarem cada canal separadamente, quadro-a-quadro. Seguinte à etapa de classificação das CNNs, um classificador SVM foi aplicado para avaliar a concatenação dos vetores de saída das CNNs e, por fim, classificar os quadros entre queda e não-queda. Os parâmetros do SVM foram os seguintes: função {\it kernel} de base radial, regularização ou $C$ igual a 1, mesmo peso para ambas as classes e gama ($\gamma$) conforme definido na Equação~\ref{eq:gamma}.
\begin{equation}
\label{eq:gamma}
\gamma = \frac{1}{\textit{número de características * variância da entrada}}
\end{equation}