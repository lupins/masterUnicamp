\chapter{Introdução}
\label{chap:introduction}

A expectativa de vida, em países desenvolvidos, alcançou valores acima de 60 anos~\cite{who2011global}. Alguns países da União Europeia e China esperam que 20\% de sua população atinjam mais de 65 anos até 2030~\cite{who2007world}. Segundo a Organização Mundial da Saúde~\cite{who2007world,who2015ageing}, esse recente patamar é um efeito colateral de avanços científicos, descobertas médicas, redução da mortalidade infantil e mudanças culturais.

Entretanto, embora os seres humanos estejam vivendo mais, a qualidade deste tempo de vida é definida principalmente pela saúde. Como a saúde dita a independência, a satisfação e a possibilidade de engajar em atividades importantes para o bem-estar do idoso, diversos grupos de pesquisa direcionaram seu foco para tecnologias de manutenção da saúde na terceira idade.

\section{Motivação}

Naturalmente, ao longo do envelhecimento aparecem problemas de saúde, principalmente em decorrência das limitações biológicas do corpo humano e do enfraquecimento muscular. Esse enfraquecimento em idades avançadas aumenta as chances de um individuo sofrer uma queda. Em termos de acidentes domésticos, as quedas são a segunda maior causa de morte ao redor do mundo, com números em torno de 646.000 mortes por ano~\cite{who2007world}. Relatórios apontam que entre 28\% e 35\% da população com mais de 65 anos sofrem pelo menos uma queda ao ano e este percentual sobe para 32\% a 42\% em idosos com mais de 70 anos.

Em uma tentativa de agrupar os eventos que podem levar a uma queda, Lusardi et al.~\cite{lusardi2017determining} relataram os fatores de risco, a forma como as quedas ocorrem, quem as sofre e algumas precauções para evitá-las. Mesmo em casos em que ocorre uma pequena queda, ela pode quebrar ou trincar ossos e machucar tecidos moles que, devido à idade avançada, podem não se recuperar completamente. Isso causa uma cadeia de danos fisiológicos e psicológicos, consequentemente diminuindo a auto-confiança e independência do idoso.

Assim como crianças, os idosos necessitam de cuidados e acompanhamento constantes para evitar ferimentos graves, especialmente no que diz respeito ao tempo entre a ocorrência do acidente e o início dos cuidados adequados. Uma das medidas efetivas é a contratação de cuidadores qualificados para acompanharem o idoso em tempo integral. Porém, em particular nos países desenvolvidos, onde serviços são caros, este custo com profissionais somado a outros orçamentos elevado da vida idosa, como médicos e medicamentos, acaba sendo inviabilizado. Em geral, esses idosos são realocados para a casa de seus familiares próximos, o que diminui sua privacidade e independência.

Com base no cenário descrito anteriormente, pode-se arquitetar uma solução tecnológica na forma de um sistema de emergência que, de forma confiável, acionaria a assistência qualificada automaticamente. Dessa maneira, o tempo de resposta entre o acidente e os cuidados seria reduzido, possibilitando ao idoso habitar em sua própria residência, mantendo sua independência e auto-estima. O diagrama da Figura~\ref{fig:ems_system} ilustra os componentes de um sistema de assistência ao dia-a-dia (do inglês ADL ou \textit{Assisted Daily Living}).

\begin{figure}[!htb]
\centering
\includegraphics[width=13.7cm]{figures/intro_emergency_system}
\caption[Visão geral do sistema de emergência]{Diagrama com os principais componentes do sistema de computação de emergência para monitoramento de acidentes.}
\label{fig:ems_system}
\end{figure}

O sistema é alimentado por uma gama de sensores, instalados na residência ou aderidos ao corpo do idoso, que monitoram informações como a pressão sanguínea, número de batimentos cardíacos, temperatura, níveis de glicose no sangue, aceleração e postura. Esses dispositivos são conectados a um centro de processamento local que, além de construir continuamente relatórios técnicos da saúde do idoso, também aciona um operador remoto em caso de emergência. Ao receber o alerta de emergência, o operador realiza uma verificação da situação e, ao constatar a necessidade, aciona o socorro médico.

\section{Objetivos}

A partir das motivações acima, este trabalho foca na utilização de uma abordagem multicanal, aliada a técnicas de aprendizado profundo, para a detecção de quedas humanas em sequências de vídeos.

Um algoritmo capaz de realizar essa detecção de forma automática é um módulo importante no sistema de emergência da Figura~\ref{fig:ems_system} e, para tal, os seguintes objetivos foram definidos:

\begin{enumerate}
\item Definição de um patamar base na literatura relacionada.
\item Preparação das bases de dados.
\item Extensão da arquitetura de patamar base.
\item Avaliação dos multicanais de informações.
\item Avaliação do modelo proposto.
\item Publicação dos resultados.
\end{enumerate}

\section{Questões de Pesquisa}
\label{sec:questoes_pesquisa}

Durante o cumprimento dos objetivos definidos anteriormente, pretendemos responder às seguintes questões de pesquisa:

\begin{enumerate}

\item Os multicanais são capazes de manter informação temporal o suficiente para que uma rede aprenda seus padrões?

\item Quais canais contribuem melhor ao problema de detectar quedas?

\item A eficácia do método proposto se mantém para outras bases de dados contendo quedas humanas?

\item As arquiteturas tridimensionais (3D) são mais discriminativas do que as arquiteturas bidimensionais (2D) para este problema?

\end{enumerate}

\section{Contribuições}

As principais contribuições deste trabalho são dois modelos multicanais para a detecção de quedas humanas, testados em duas bases de dados e disponíveis publicamente. Além disso, nós apresentamos uma extensa comparação entre os canais e as arquiteturas empregadas, cujos resultados são comparáveis ao estado da arte, bem como uma discussão sobre a utilização de conjuntos de dados simulados.

\section{Lista de Publicações}

Os seguintes artigos~\cite{carneiro2019multi,carneiro2019deep,leite2019fall} foram publicados durante a realização deste trabalho de pesquisa, cujos resultados estão diretamente relacionados ao tema investigado nesta dissertação:

\begin{itemize}

\item G.V. Leite, G.P. Silva, H. Pedrini. {\it Fall Detection in Video Sequences Based on a Three-Stream Convolutional Neural Network.} 18th IEEE International Conference on Machine Learning and Applications (ICMLA), pp. 191--195, Boca Raton-FL, USA, December 16-19, 2019.

\item S.A. Carneiro, G.P. Silva, G.V. Leite, R. Moreno, S.J.F. Guimar{\~a}es, H. Pedrini.  {\it Deep Convolutional Multi-Stream Network Detection System Applied to Fall Identification in Video Sequences.} 15th International Conference on Machine Learning and Data Mining (MLDM), pp. 681-695, New York-NY, USA, July 20-24, 2019.

\item S.A. Carneiro, G.P. Silva, G.V. Leite, R. Moreno, S.J.F. Guimar{\~a}es, H. Pedrini. {\it Multi-Stream Deep Convolutional Network Using High-Level Features Applied to Fall Detection in Video Sequences.} 26th International Conference on Systems, Signals and Image Processing (IWSSIP), pp. 293-298, Osijek, Croatia, June 05-07, 2019.

\end{itemize}

\section{Organização do Texto}

O restante deste trabalho está estruturado da seguinte forma. O Capítulo~\ref{chap:background} apresenta os principais conceitos que foram utilizados na implementação do trabalho, bem como abordagens existentes na literatura relacionadas ao tema investigado. O Capítulo~\ref{chap:method} descreve em detalhes a metodologia proposta e suas etapas. O Capítulo~\ref{chap:experiment} descreve os conjuntos de dados utilizados nos experimentos, as métricas de avaliação selecionadas, os experimentos realizados, seus resultados e discussões. Finalmente, o Capítulo~\ref{chap:conclusion} apresenta algumas considerações finais e propostas para trabalhos futuros.
