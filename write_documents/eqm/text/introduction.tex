\chapter{Introduction} \label{cap:introduction}

This chapter presents the problem to be investigated, the main objectives, contributions, research questions, as well as the text structure.

\section{Problem Characterization}

The life expectancy of humans in the developed world is currently more than 60 years~\cite{who2011global}. For instance, countries in the European Union and China expect 20\% of their population to be over 65 by 2030~\cite{who2007}. This is a side effect of reduced child mortality, increased life expectancy, advances in medicine, science, and cultural changes~\cite{who2007,who2015ageing}. Not only are individuals living longer, but the population as a whole has had an average increase in age. Although humans are living longer, their experience with this extra time is mostly defined by their health, which will dictate how a person will be able to engage in activities, that are important to their well being, independence, and personal satisfaction.

Health problems will naturally rise alongside aging, mainly due to the biological limitations of the human body and muscle weakening. The muscular weakening in advanced ages facilitates the occurrence of falls, which are the second most common form of accidental death worldwide, accounting for the death of about 646,000 people around the world~\cite{who2007}. A small fall can break, fracture, or even cause soft tissue damage, which can never heal completely, diminishing an individual's self-confidence and independence. According to a World Health Organization report~\cite{who2017reflecting}, between 28 and 35\% of people over 65 years old fall at least once each year, and that percentage increases to 32-42\% for people over 70 years. In an attempt to clarify the many causes of a fall, Lusardi et al.~\cite{lusardi2017determining} reported several factors related to fall risk, such as how falls usually occur, to whom and how to avoid them.

To prevent serious injuries, nowadays, people rely on hiring full-time companions to care and accompany an elderly, preventing them from injuring themselves. Particularly in the developed world, where the workforce is expensive, this professional cost is a significant increase in the already large budget required to keep someone healthy at such an advanced age and it is rarely the path that is taken, most commonly the surrounding family will relocate the elderly to the family's home, striping the elderly from their independence and privacy.

A technological solution to this problem comes in the form of an emergency system that could, reliably, trigger aid in the event of a fall; thus allowing the elderly population to inhabit their own home without the longing health risk caused by falling. The solutions found in the literature revolve around attaching elders with devices, around their wrist or torso, which will measure changes in acceleration and inclination, accusing a fall after a defined threshold; which is not an acceptable solution, since the elderly have complained that they either forget to use the device or that the device is a nuisance to them~\cite{kukharenko2017}.

This proposal presents a non-intrusive solution to this problem. With the use of cameras, it is possible to implement a cheap and transparent solution for the elderly, which includes a system to call for help when a fall is automatically detected, decreasing the risk of the elderly living alone, giving back to this growing population their independence, freedom, and confidence, providing an alternative to one of the greatest hassles in advanced age.

\section{Objectives and Contributions}

This proposal focuses on the application of machine learning techniques to the problem of fall classification in video sequences. The selected baseline utilizes transfer learning to extract pre-trained weights from the work of Núñez-Marcos et al.~\cite{nunezmarcos2017}, followed by an exploration of the proposed methods to enhance the classification.

In order to accomplish this task, the following steps will be taken:

\begin{myenumerate}

\item investigation of recent work on the subject.
\item dataset preparation.
\item reproduction of the baseline results.
\item extension of the baseline architecture based on approaches that achieved successful results on related problems.
\item performance evaluation of the new modified model.
\item publication of the results.

\end{myenumerate}

Expected contributions of this proposal revolve around two main points: (i) a three-stream solution, in which the classification deep neural network (DNN), consisting of three DNNs, each using a pre-processing technique as input, such as optical flow, pose estimation, and saliency map, is able to provide a competitive accuracy among the state of the art reviewed solutions; and (ii) to pave the way for the creation of a computing emergency system, as illustrated in Figure~\ref{fig:emergency}, capable of raising emergency alerts to dispatch qualified help in a detected fall, through the junction of various inputs, such as a video system alert, accelerometer information, blood pressure, heartbeat, and body temperature.

\begin{figure}[!htb]
\centering
\includegraphics[width=16.0cm]{emergency}
\caption{Diagram with the main components of the emergency computing system for monitoring activities and risk for the elderly people.}
\label{fig:emergency}
\end{figure}

We intend to construct a deep learning model to achieve high accuracy rates and perform data augmentation to boost our model performance. An extensive experimental evaluation on several challenging datasets is planned to assess the effectiveness (accuracy) and efficiency (speed-up and scale-up improvements) obtained with our approach.

\section{Research Questions}

In this section, we present some research questions that motivate our dissertation proposal:

\begin{myitemize}

\item Can handcrafted features maintain sufficient motion information so that deep networks are still able to extract complex temporal patterns from them?

\item Is it possible to improve recognition accuracy by exploring multiple streams of information?

\item How can we combine characteristics of distinctive nature to improved classification accuracy?

\item Can the proposed method present successful performance across different datasets?

\item Can this model be extended to different recognition scenarios?

\end{myitemize}

\section{Text Organization}

This project is structured as follows. Chapter~\ref{cap:background} describes key concepts that will be used throughout the project implementation, as well as presents recent and relevant related work. Chapter~\ref{cap:methodology} describes the proposed methodology alongside the datasets we intend to test our implementation and the selected evaluation metrics. Chapter~\ref{cap:schedule}, we outline our work plan and schedule. In Chapter~\ref{cap:final}, we present some concluding considerations.
